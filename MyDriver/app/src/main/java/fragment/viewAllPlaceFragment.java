package fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.user.mydriver.BackgroundTask.PlaceRetrieve_BgroundTask;
import com.example.user.mydriver.Data.Place;
import com.example.user.mydriver.R;
import com.example.user.mydriver.dbFavouritePlaceOperations;

public class viewAllPlaceFragment extends Fragment {

    public static ListView place_listview;

    public viewAllPlaceFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_addplace, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.action_addplace: {
                addPlaceFragment addPlaceFragment = new addPlaceFragment("Favourite", 0);
                this.getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_Container, addPlaceFragment, "AddPlace")
                        .addToBackStack(null)
                        .commit();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_all_place, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceStated){
        super.onActivityCreated(savedInstanceStated);

        place_listview = (ListView)getActivity().findViewById(R.id.display_placeListView);
        PlaceRetrieve_BgroundTask favouritePlaceRetrieveBgroundTask = new PlaceRetrieve_BgroundTask(getContext());
        favouritePlaceRetrieveBgroundTask.execute("get_Info");

        place_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final Place place = (Place) place_listview.getItemAtPosition(position);

                new AlertDialog.Builder(getContext())
                        .setTitle("Edit " + place.getPlace_Name())
                        .setMessage("Are you sure you want to edit " + place.getPlace_Name() + " ?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                int pid = place.getPlace_ID();

                                if (place.getPlace_Name().equals("Home")) {
                                    addPlaceFragment addPlaceFragment = new addPlaceFragment("Edit Home", pid);
                                    getFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_Container, addPlaceFragment, "EditHome")
                                        .addToBackStack(null)
                                        .commit();
                                }
                                else if (place.getPlace_Name().equals("Work")) {
                                    addPlaceFragment addPlaceFragment = new addPlaceFragment("Edit Work", pid);
                                    getFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_Container, addPlaceFragment, "EditWork")
                                        .addToBackStack(null)
                                        .commit();
                                }
                                else if (place.getPlace_Name().equals("Favourite")) {
                                    addPlaceFragment addPlaceFragment = new addPlaceFragment("Edit Favourite", pid);
                                    getFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_Container, addPlaceFragment, "EditFavourite")
                                        .addToBackStack(null)
                                        .commit();
                                }
                                else {
                                    //nothing
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return false;
            }
        });
    }
}
