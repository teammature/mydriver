package fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.user.mydriver.DirectionsJSONParser;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.example.user.mydriver.Data.Reservation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class BookingFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback{

    private Fragment fragment = this;
    GoogleMap mMap;
    SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();
    private String distance = "";
    private String duration = "";
    private Button mBtnConfirm;
    private EditText mEt_StartTime, mEt_EndTime, mEt_Date;
    private static EditText mEt_Origin, mEt_Destination;
    private Reservation reservation;
    private ToggleButton tg_button;
    private String Status, m_Date;
    private String m_text_StartTime, m_text_EndTime, m_text_origin, m_text_destination;
    ParseUser currentUser = ParseUser.getCurrentUser();
    private Marker destMarker, pickupMarker, curMarker;
    private Polyline mapLine;
    Date date = null;
    LatLngBounds.Builder builder = new LatLngBounds.Builder();

    public BookingFragment(String status, String username, String date, String startTime){
        Status = status;
        Log.i("SEND MAJOR HELP", date);
        Log.i("SEND MAJOR HELP", startTime);

        m_Date = date;
        m_text_StartTime = startTime;
    }

    public BookingFragment(String status, Reservation reservation, String username, String date, String startTime){
        Status = status;
        this.reservation = reservation;

        Log.i("SEND MAJOR HELP", date);
        Log.i("SEND MAJOR HELP", startTime);

        m_Date = date;
        m_text_StartTime = startTime;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_booking, null, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        MainActivity.toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        MainActivity.activity.setSupportActionBar(MainActivity.toolbar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceStated){
        super.onActivityCreated(savedInstanceStated);

        mBtnConfirm = (Button)getActivity().findViewById(R.id.btnConfirm);

        mEt_Origin = (EditText)getActivity().findViewById(R.id.et_From);
        mEt_Destination = (EditText)getActivity().findViewById(R.id.et_To);
        mEt_Date = (EditText)getActivity().findViewById(R.id.et_Date);
        mEt_StartTime = (EditText)getActivity().findViewById(R.id.et_StartTime);
        mEt_EndTime = (EditText) getActivity().findViewById(R.id.et_EndTime);
        tg_button = (ToggleButton)getActivity().findViewById(R.id.tb_way);

        if (reservation != null){
            mEt_Origin.setText(reservation.getOrigin());
            mEt_Destination.setText(reservation.getDestination());
            mEt_Date.setText(reservation.getDate());
            mEt_StartTime.setText(reservation.getStartTime());
            mEt_EndTime.setText(reservation.getEndTime());

            if (reservation.getOneWay())
                tg_button.setChecked(true);
        }
        else {
            if (!m_Date.equals("null"))
                mEt_Date.setText(m_Date);

            if (!m_text_StartTime.equals("null"))
                mEt_StartTime.setText(m_text_StartTime);
        }

        tg_button.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {
                if (isChecked)
                    mEt_EndTime.setEnabled(false);
                else
                    mEt_EndTime.setEnabled(true);
            }
        });

        //Date Picker
        mEt_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                DatePickerFragment picker = new DatePickerFragment(1);
                picker.show(fm, "Date Picker");
            }
        });

        //Start Time Picker
        mEt_StartTime = (EditText) getActivity().findViewById(R.id.et_StartTime);

        mEt_StartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                TimePickerFragment timepicker = new TimePickerFragment(1);
                timepicker.show(fm, "Start Time Picker");
            }
        });

        //End Time Picker
        mEt_EndTime = (EditText) getActivity().findViewById(R.id.et_EndTime);

        mEt_EndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                TimePickerFragment timepicker = new TimePickerFragment(2);
                timepicker.show(fm, "End Time Picker");
            }
        });

        mEt_Origin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDLocation_DialogFragment locationpicker = new PDLocation_DialogFragment(1);
                locationpicker.setTargetFragment(BookingFragment.this, 1);
                locationpicker.show(getFragmentManager().beginTransaction(), "Pick Up");
            }
        });

        mEt_Destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDLocation_DialogFragment locationpicker = new PDLocation_DialogFragment(2);
                locationpicker.setTargetFragment(BookingFragment.this, 2);
                locationpicker.show(getFragmentManager().beginTransaction(), "Drop Off");
            }
        });

        //button to confirm the booking
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //extract data
                m_Date = mEt_Date.getText().toString();
                m_text_StartTime = mEt_StartTime.getText().toString();
                m_text_EndTime = mEt_EndTime.getText().toString();

                if (mEt_Origin.getText().toString().trim().length() == 0)
                    m_text_origin = "null";
                else
                    m_text_origin = mEt_Origin.getText().toString();

                if (mEt_Destination.getText().toString().trim().length() == 0)
                    m_text_destination = "null";
                else
                    m_text_destination = mEt_Destination.getText().toString();

                //------- convert string to date
                try {
                    date = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(m_Date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);

                if (Status.equals("EditBooking")){
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");

                    query.getInBackground(reservation.getObject_Id(), new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, com.parse.ParseException e) {
                            if (e == null){
                                parseObject.put("r_Passenger_name", currentUser.getUsername());
                                parseObject.put("r_isOneWay", tg_button.isChecked());
                                parseObject.put("Date", calendar.getTime());
                                parseObject.put("r_Origin", m_text_origin);
                                parseObject.put("r_Destination", m_text_destination);
                                parseObject.put("r_StartTime", m_text_StartTime);
                                parseObject.put("r_EndTime", m_text_EndTime);
                                parseObject.put("isCompleted", false);

                                parseObject.saveInBackground();
                            }
                        }
                    });
                }

                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Booking ?")
                            .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //parse object
                                    //sending to server

                                    final ParseObject newReservation = new ParseObject("Reservation");
                                    newReservation.put("r_Passenger_name", currentUser.getUsername());
                                    newReservation.put("r_isOneWay", tg_button.isChecked());
                                    newReservation.put("Date", calendar.getTime());
                                    newReservation.put("r_Origin", m_text_origin);
                                    newReservation.put("r_Destination", m_text_destination);
                                    newReservation.put("r_StartTime", m_text_StartTime);
                                    newReservation.put("r_EndTime", m_text_EndTime);
                                    newReservation.put("isCompleted", false);

                                    newReservation.saveInBackground();

                                    Toast.makeText(getActivity(), "Booking has been confirmed", Toast.LENGTH_SHORT).show();

                                    //TODO: return to previous fragment
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(), "Booking has been cancelled", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .show();
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("RequestCode", "Code " + requestCode);
        switch (requestCode){
            case 1:{
                if (resultCode == Activity.RESULT_OK){
                    Bundle bundle = data.getExtras();
                    String address = bundle.getString("FinalAddress");
                    List<Address> addresses = bundle.getParcelableArrayList("Address");
                    mEt_Origin.setText(address);
                    onSearchPickupLocation(addresses);
                }
                break;
            }
            case 2: {
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String address = bundle.getString("FinalAddress");

                    Log.d("Destination", "Address " +address);
                    List<Address> addresses = bundle.getParcelableArrayList("Address");
                    mEt_Destination.setText(address);
                    onSearchDestination(addresses);
                }
                break;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);

        buildGoogleApiClient();

        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(this,"onConnected",Toast.LENGTH_SHORT).show();
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            //place marker at current position
            mMap.clear();
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            curMarker = mMap.addMarker(markerOptions);
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location) {
        //remove previous current location marker and add new one at current position

        if (curMarker != null){
            curMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
        curMarker = mMap.addMarker(markerOptions);

        //Toast.makeText(this,"Location Changed",Toast.LENGTH_SHORT).show();
    }

    //Origin latitude, Origin Longtitude, Destination Latitude, Destionation Longtitude
    private String getDirectionsUrl(double mOriginLatitude, double mOriginLongtitude, double mDestinationLatitude, double mDestinationLongtitude){

        LatLng origin = new LatLng(mOriginLatitude, mOriginLongtitude);
        LatLng dest = new LatLng(mDestinationLatitude, mDestinationLongtitude);

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception download url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            if (mapLine != null){
                mapLine.remove();
                mapLine = null;
            }

            if(result.size()<1){
                Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }


                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(Color.CYAN);
                lineOptions.geodesic(true);
            }

            //Drawing polyline in the Google Map for the i-th route
            mapLine = mMap.addPolyline(lineOptions);
        }
    }

    //get latitude and longtitude from LocationFragment name
    public void onSearchDestination(List<Address> addressList) {

        if (destMarker != null) {
            destMarker.remove();
            destMarker = null;
        }

        Address address = addressList.get(0);
        double latitude = address.getLatitude();
        double longitude = address.getLongitude();
        LatLng latLng = new LatLng(latitude,longitude);

        MarkerOptions marker = new MarkerOptions()
                .position(latLng)
                .title("Going Here ?")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location));

        destMarker  = mMap.addMarker(marker);

        if (destMarker != null && pickupMarker != null) {
            builder.include(destMarker.getPosition());
            builder.include(pickupMarker.getPosition());

            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            mMap.animateCamera(cu);

            LatLng pickup = pickupMarker.getPosition();
            LatLng dest = destMarker.getPosition();


            String url = getDirectionsUrl(pickup.latitude, pickup.longitude, dest.latitude, dest.longitude);
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);
        }
        else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        }
    }

    public void onSearchPickupLocation(List<Address> addressList) {

        if (pickupMarker != null) {
            pickupMarker.remove();
            pickupMarker = null;
        }

        Address address = addressList.get(0);
        double latitude = address.getLatitude();
        double longitude = address.getLongitude();
        LatLng latLng = new LatLng(latitude,longitude);

        MarkerOptions marker = new MarkerOptions()
                .position(latLng)
                .title("Pick Up From here ?")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location));

        pickupMarker  = mMap.addMarker(marker);

        if (destMarker != null && pickupMarker != null) {
            builder.include(destMarker.getPosition());
            builder.include(pickupMarker.getPosition());

            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            mMap.animateCamera(cu);

            LatLng pickup = pickupMarker.getPosition();
            LatLng dest = destMarker.getPosition();


            String url = getDirectionsUrl(pickup.latitude, pickup.longitude, dest.latitude, dest.longitude);
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);
        }
        else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
        }
    }

    public void setPickUpTextBox(String location){
        mEt_Origin.setText(location);
    }

    public void setDropOffTextBox(String location){
        mEt_Destination.setText(location);
    }
}
