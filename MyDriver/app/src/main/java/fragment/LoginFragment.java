package fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class LoginFragment extends Fragment {

    private OnLoginFragmentInteractionListener mListener;
    private ParseUser currentUser;
    private String luserName, lpassword;
    private Dialog progressDialog;
    private Button lbtnLogin;
    private EditText let_userName, let_password;
    private TextView lbtnSignUp, lbtnFgtPwd, tv_username, tv_password;
    private Boolean isNameEmpty = true, isPasswordEmpty = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_login, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        lbtnSignUp = (TextView) getActivity().findViewById(R.id.lbtn_Register);
        lbtnFgtPwd = (TextView)getActivity().findViewById(R.id.lbtn_fgtPwd);
        tv_username = (TextView) getActivity().findViewById(R.id.lgusername);
        tv_password = (TextView)getActivity().findViewById(R.id.lgpassword);
        lbtnLogin = (Button) getActivity().findViewById(R.id.lbtn_Login);

        let_userName = (EditText) getActivity().findViewById(R.id.et_Name);
        let_password = (EditText) getActivity().findViewById(R.id.et_Password);

        lbtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickRegisterButton();
            }
        });

        lbtnFgtPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickForgetPwd();
            }
        });

        let_userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tv_username.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        let_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tv_password.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        let_password.setOnEditorActionListener(new AutoCompleteTextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    final String luserName = let_userName.getText().toString();
                    final String lpassword = let_password.getText().toString();

                    if (luserName.trim().length() == 0){
                        tv_username.setVisibility(View.VISIBLE);
                        isNameEmpty = true;
                    }
                    else{
                        isNameEmpty = false;
                    }

                    if (lpassword.trim().length() == 0){
                        tv_password.setVisibility(View.VISIBLE);
                        isPasswordEmpty = true;
                    }
                    else isPasswordEmpty = false;

                    if (!isNameEmpty && !isPasswordEmpty) {
                        showProgressBar();
                        ParseUser.logInInBackground(luserName, lpassword, new LogInCallback() {
                            public void done(ParseUser user, ParseException e) {
                                if (user != null) {
                                    currentUser = user;
                                    dismissProgressBar();
                                    Toast.makeText(getActivity(), "Login Successful", Toast.LENGTH_SHORT).show();
                                    mListener.onClickLoginSuccess(currentUser);

                                } else {
                                    dismissProgressBar();
                                    Toast.makeText(getActivity().getBaseContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        return true;
                    }
                }
                return false;
            }
        });

        lbtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking
                luserName = let_userName.getText().toString();
                lpassword = let_password.getText().toString();

                if (luserName.trim().length() == 0){
                    tv_username.setVisibility(View.VISIBLE);
                    isNameEmpty = true;
                }
                else{
                    isNameEmpty = false;
                }

                if (lpassword.trim().length() == 0){
                    tv_password.setVisibility(View.VISIBLE);
                    isPasswordEmpty = true;
                }
                else isPasswordEmpty = false;

                if (!isNameEmpty && !isPasswordEmpty){
                    showProgressBar();
                    ParseUser.logInInBackground(luserName, lpassword, new LogInCallback() {
                        public void done(ParseUser user, ParseException e) {
                            if (user != null) {
                                dismissProgressBar();
                                currentUser = user;
                                Toast.makeText(getActivity(), "Login Successful", Toast.LENGTH_SHORT).show();
                                mListener.onClickLoginSuccess(currentUser);
                            } else {
                                dismissProgressBar();
                                Toast.makeText(getActivity(), "Login Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    public void showProgressBar(){
        progressDialog = ProgressDialog.show(getContext(), "", "Logging in", true);
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentInteractionListener) {
            mListener = (OnLoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnLoginFragmentInteractionListener {
        // TODO: Update argument type and name
        void onClickLoginSuccess(ParseUser user);
        void onClickRegisterButton();
        void onClickForgetPwd();
    }
}
