package fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.user.mydriver.BackgroundTask.CheckSchedule_BgroundTask;
import com.example.user.mydriver.Data.ExistingBooking;
import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.DirectionsJSONParser;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Owner on 6/6/2016.
 */
public class temp_fragment extends Fragment implements
        Animation.AnimationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback{

    private RelativeLayout rl;
    private Animation animShow, animHide;
    private Button btn_cfmBooking;
    private Menu menu;
    private String distance = "";
    private String duration = "";
    private static EditText bet_Origin, bet_Destination;
    private Marker destMarker, pickupMarker, curMarker;
    private Polyline mapLine;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();
    private Reservation reservation;
    private String status;
    private String pickup_FullAddress, dropoff_FullAdress;
    Date date = null;
    Date comparedDate = null;
    private boolean isAvailable = true, getRoute = true;
    private boolean isPickupEmpty = true, isDropoffEmpty = true;
    private Dialog progressDialog;
    private OnFragmentInteractionListener mListener;
    ParseUser currentUser = ParseUser.getCurrentUser();

    public temp_fragment(){}

    public temp_fragment(Reservation r, String s){
        reservation = r;
        status = s;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_getlocation, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_getLocation: {

                if (rl.getVisibility() == View.GONE) {
                    expandView();
                }
                else if (rl.getVisibility() == View.VISIBLE){
                    collapseView();
                }
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void collapseView() {
        MenuItem item = menu.findItem(R.id.action_getLocation);
        item.setIcon(getResources().getDrawable(R.drawable.ic_expand_more_white_24dp));

        rl.startAnimation(animHide);
        rl.setVisibility(View.GONE);
    }

    private void expandView() {
        MenuItem item = menu.findItem(R.id.action_getLocation);
        item.setIcon(getResources().getDrawable(R.drawable.ic_expand_less_white_24dp));

        rl.startAnimation(animShow);
        rl.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.temp_fragment_booking, null, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        MainActivity.toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        MainActivity.activity.setSupportActionBar(MainActivity.toolbar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceStated){
        super.onActivityCreated(savedInstanceStated);

        //------------------------------------------------------------------------------------------

        bet_Origin = (EditText)getActivity().findViewById(R.id.bet_pickup);
        bet_Destination = (EditText)getActivity().findViewById(R.id.bet_dropoff);

        rl = (RelativeLayout)getActivity().findViewById(R.id.view_location_info);
        animShow = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
        animHide = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);

        animShow.setAnimationListener(this);

        if (status.equalsIgnoreCase("Edit")){
            bet_Origin.setText(reservation.getbDetails_OriginName());
            bet_Destination.setText(reservation.getbDetails_DestinationName());

            LatLng origin = getLatLngFromAddress(reservation.getOrigin());
            LatLng dest = getLatLngFromAddress(reservation.getDestination());

            String url = getDirectionsUrl(origin.latitude, origin.longitude, dest.latitude, dest.longitude);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }

        bet_Origin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDLocation_DialogFragment locationpicker = new PDLocation_DialogFragment(1);
                locationpicker.setTargetFragment(temp_fragment.this, 1);
                locationpicker.show(getFragmentManager().beginTransaction(), "Pick Up");
            }
        });

        bet_Destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDLocation_DialogFragment locationpicker = new PDLocation_DialogFragment(2);
                locationpicker.setTargetFragment(temp_fragment.this, 2);
                locationpicker.show(getFragmentManager().beginTransaction(), "Drop Off");
            }
        });

        btn_cfmBooking = (Button)getActivity().findViewById(R.id.btn_location_confirm);

        btn_cfmBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAvailable = true;

                if (bet_Origin.getText().toString().trim().length() == 0){
                    bet_Origin.setHint("Pick Up Location Required");
                    bet_Origin.setHintTextColor(Color.RED);
                    isPickupEmpty = true;
                }
                else isPickupEmpty = false;

                if (bet_Destination.getText().toString().trim().length() == 0){
                    bet_Destination.setHint("Drop Off Location Required");
                    bet_Destination.setHintTextColor(Color.RED);
                    isDropoffEmpty = true;
                }
                else isDropoffEmpty = false;

                if (!isPickupEmpty && !isDropoffEmpty) {
                    reservation.setbDetails_OriginName(bet_Origin.getText().toString());
                    reservation.setbDetails_DestinationName(bet_Destination.getText().toString());

                    //------- convert string to date
                    try {
                        date = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(reservation.getDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    final Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.DATE, 1);

                    reservation.setbDetails_Date(calendar.getTime().toString());

                    //-------- calculate estimated pick up time
                    String estimate_pickup = calculateEstimatedPickUpTime();
                    reservation.setbDetails_EstimatedStartTripTime(estimate_pickup);

                    String estimate_end = "";

                    if (!reservation.getOneWay()) {
                        estimate_end = calculateEstimatedEndTime();
                        reservation.setbDetails_EstimatedEndTripTime(estimate_end);
                    } else {
                        reservation.setbDetails_EndTime(calculateEndTime().toString());
                        reservation.setbDetails_EstimatedEndTripTime(calculateEndTime().toString());
                    }

                    getRoute = false;

                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    retrieveSchedule(calendar.getTime());
                                }
                            });
                        }
                    };
                    Thread thread = new Thread(r);
                    thread.start();

                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    getRoute = true;
                }
            }
        });
    }

    public void makeBooking(){

        //------- convert string to date
        try {
            date = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(reservation.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);

        //-------------------------------start booking or editing---------------------------
        if (isAvailable && status.equalsIgnoreCase("Add")) {
            Log.d("Checking", "isAvailable 1 = " + isAvailable);

            ParseObject newReservation = new ParseObject("Reservation");
            newReservation.put("r_Passenger_name", currentUser.getUsername());
            newReservation.put("r_isOneWay", reservation.getOneWay());

            //------- save reservation to parse
            newReservation.put("Date", calendar.getTime());
            newReservation.put("r_OriginName", reservation.getbDetails_OriginName());
            newReservation.put("r_Origin", reservation.getOrigin());
            newReservation.put("r_DestinationName", reservation.getbDetails_DestinationName());
            newReservation.put("r_Destination", reservation.getDestination());
            newReservation.put("r_StartTime", reservation.getStartTime());//actual start time
            newReservation.put("r_EstimatedPickUpTime", reservation.getbDetails_EstimatedStartTripTime());
            newReservation.put("r_EstimatedEndTripTime", reservation.getbDetails_EstimatedEndTripTime());
            newReservation.put("r_EndTime", reservation.getEndTime());//actual end time
            newReservation.put("r_Note", reservation.getbDetails_Note());
            newReservation.put("isCompleted", false);

            newReservation.saveInBackground();

            dismissProgressBar();

            Toast.makeText(getActivity(), "Successfully Booked", Toast.LENGTH_LONG).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Trip Info");
            builder.setMessage("Distance : " + distance + "\nDuration : " + duration
                    + "\nEstimated Pick Up Time : " + reservation.getbDetails_EstimatedStartTripTime());

            // Set up the buttons
            builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    mListener.onBookingConfirmed();
                }
            });
            builder.show();
        }

        else if (isAvailable && status.equalsIgnoreCase("Edit")){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");

            query.getInBackground(reservation.getObject_Id(), new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, com.parse.ParseException e) {
                    if (e == null){
                        Log.d("Object ID", "Checking object Id = " + reservation.getObject_Id());
                        Log.d("Object ID", "Checking parse object Id = " + parseObject.getObjectId());

                        parseObject.put("r_Passenger_name", currentUser.getUsername());
                        parseObject.put("r_isOneWay", reservation.getOneWay());
                        parseObject.put("Date", calendar.getTime());
                        parseObject.put("r_OriginName", reservation.getbDetails_OriginName());
                        parseObject.put("r_Origin", reservation.getOrigin());
                        parseObject.put("r_DestinationName", reservation.getbDetails_DestinationName());
                        parseObject.put("r_Destination", reservation.getDestination());
                        parseObject.put("r_StartTime", reservation.getStartTime());//actual start time
                        parseObject.put("r_EstimatedPickUpTime", reservation.getbDetails_EstimatedStartTripTime());
                        parseObject.put("r_EstimatedEndTripTime", reservation.getbDetails_EstimatedEndTripTime());
                        parseObject.put("r_EndTime", reservation.getEndTime());//actual end time
                        parseObject.put("r_Note", reservation.getbDetails_Note());
                        parseObject.put("isCompleted", false);

                        parseObject.saveInBackground();
                    }
                }
            });
            Toast.makeText(getActivity(), "Successfully Updated", Toast.LENGTH_LONG).show();


            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Trip Info");
            builder.setMessage("Distance : " + distance + "\nDuration : " + duration
                    + "\nEstimated Pick Up Time : " + reservation.getbDetails_EstimatedStartTripTime());

            // Set up the buttons
            builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    mListener.onBookingConfirmed();
                }
            });
            builder.show();
        }
        else {
            Toast.makeText(getActivity(), "The Time Slot selected is clashing with another booking. \n" +
                    "Please select another Time Slot.", Toast.LENGTH_SHORT).show();
        }
    }

    //---calculate estimated end trip for return
    private String calculateEstimatedEndTime(){
        int hour = 0;
        int min = 0;

        String[] splited = duration.split("\\s+");

        if (duration.contains("hour")) {
            int i = duration.indexOf(' ');
            hour = Integer.parseInt(splited[0]);
            min = Integer.parseInt(splited[2]);
        } else {
            min = Integer.parseInt(splited[0]);
        }

        if (min >= 30) {
            hour = hour + 1;
            min = 0;
        } else
            min = 30;

        DateFormat tf = new SimpleDateFormat("HH:mm");
        Calendar c = Calendar.getInstance();

        try {
            c.setTime(tf.parse(reservation.getEndTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (hour > 0) {
            c.add(Calendar.HOUR, hour);
        }

        if (min > 0) {
            c.add(Calendar.MINUTE, min);
        }

        return tf.format(c.getTime());
    }

    //---calculate estimated end trip for one way
    private String calculateEndTime(){
        int hour = 0;
        int min = 0;

        String[] splited = duration.split("\\s+");

        if (duration.contains("hour")) {
            int i = duration.indexOf(' ');
            hour = Integer.parseInt(splited[0]);
            min = Integer.parseInt(splited[2]);
        } else {
            min = Integer.parseInt(splited[0]);
        }

        if (min >= 30) {
            hour = hour + 1;
            min = 0;
        } else
            min = 30;

        DateFormat tf = new SimpleDateFormat("HH:mm");
        Calendar c = Calendar.getInstance();

        try {
            c.setTime(tf.parse(reservation.getStartTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (hour > 0) {
            c.add(Calendar.HOUR, hour);
        }

        if (min > 0) {
            c.add(Calendar.MINUTE, min);
        }

        return tf.format(c.getTime());
    }

    private String calculateEstimatedPickUpTime(){
        int hour = 0;
        int min = 0;

        String[] splited = duration.split("\\s+");

        if (duration.contains("hour")) {
            int i = duration.indexOf(' ');
            hour = Integer.parseInt(splited[0]);
            min = Integer.parseInt(splited[2]);
        } else {
            min = Integer.parseInt(splited[0]);
        }

        if (min >= 30) {
            hour = hour + 1;
            min = 0;
        } else
            min = 30;

        DateFormat tf = new SimpleDateFormat("HH:mm");
        Calendar c = Calendar.getInstance();

        try {
            c.setTime(tf.parse(reservation.getStartTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (hour > 0) {
            hour = hour * -1;
            c.add(Calendar.HOUR, hour);
        }

        if (min > 0) {
            min = min * -1;
            c.add(Calendar.MINUTE, min);
        }

        return tf.format(c.getTime());
    }

    private void retrieveSchedule(Date date) {
        //-----------get current date
        showProgressBar();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(calendar.getTime());

        try {
            comparedDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(formattedDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        if (reservation.getObject_Id() == null){
            reservation.setbDetails_objectId("new");
        }
        else{
            Log.d("Checking ", "Object id = " + reservation.getObject_Id());
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");

        final List<Reservation> temp = new ArrayList<>();

        query.whereEqualTo("Date", comparedDate);
        query.whereEqualTo("isCompleted", false);
        query.whereNotEqualTo("objectId", reservation.getObject_Id());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, com.parse.ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        Log.d("Checking ", "List Object id = " + list.get(i).getString("objectId"));
                        Boolean isOneway = list.get(i).getBoolean("r_isOneWay");
                        String Origin = list.get(i).getString("r_Origin");
                        String Destination = list.get(i).getString("r_Destination");
                        String estimatedPickUpTime = list.get(i).getString("r_EstimatedPickUpTime");
                        String estimatedReturnTime = list.get(i).getString("r_EstimatedEndTripTime");
                        Reservation r = new Reservation(isOneway, Origin, Destination, estimatedPickUpTime, estimatedReturnTime);
                        temp.add(r);
                    }
                }
                dismissProgressBar();
                addReservationtoList_Check(temp);
            }
        });
    }

    public void addReservationtoList_Check(List<Reservation> listreservation){
        String EstimatedPickUpTime, EstimatedEndTripTime;
        String NextTripStartPoint, PreviousTripEndPoint;

        Log.d("Reservations size", "size = " + listreservation.size());

        for (int i = 0; i < listreservation.size(); i++){
            EstimatedEndTripTime = listreservation.get(i).getbDetails_EstimatedEndTripTime();
            EstimatedPickUpTime = listreservation.get(i).getbDetails_EstimatedStartTripTime();

            Date cur_trippicuptime = null, cur_tripendtime = null;
            Date trippickuptime = null, tripendtime = null;
            DateFormat df = new SimpleDateFormat("HH:mm");

            //convert list end trip time
            try {
                tripendtime = df.parse(EstimatedEndTripTime);// all done
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //convert list trip start time
            try {
                trippickuptime = df.parse(EstimatedPickUpTime);// all done
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //convert this trip pick up time
            try {
                cur_trippicuptime = df.parse(reservation.getbDetails_EstimatedStartTripTime());// all done
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //convert this trip end time
            try {
                cur_tripendtime = df.parse(reservation.getbDetails_EstimatedEndTripTime());// all done
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d("Checking", "Reservation " + i + " start trip = " + trippickuptime.toString());
            Log.d("Checking", "Reservation " + i + " end trip = " + tripendtime.toString());
            Log.d("Checking", "This trip start time" + cur_trippicuptime.toString());
            Log.d("Checking", "This trip end time" + cur_tripendtime.toString());

            //make sure trip is before this trip pick up time
            if (tripendtime.before(cur_trippicuptime) && trippickuptime.before(cur_trippicuptime)) {
                if (listreservation.get(i).getOneWay())
                    PreviousTripEndPoint = listreservation.get(i).getDestination();
                else
                    PreviousTripEndPoint = listreservation.get(i).getOrigin();

                checkabletopickup_thistrip(EstimatedEndTripTime, PreviousTripEndPoint);
            }
            //make sure trip is after this trip end time
            else if (tripendtime.after(cur_trippicuptime) && trippickuptime.after(cur_tripendtime)){
                NextTripStartPoint = listreservation.get(i).getOrigin();
                checkabletopickup_nexttrip(EstimatedPickUpTime, NextTripStartPoint);
            }

            //previous trip end time is after cur trip pick up but previous trip start time is before cur trip pick up
            else if (tripendtime.after(cur_trippicuptime) && trippickuptime.before(cur_trippicuptime)){
                Log.d("Checking", "Time is Crashing - 1");
                isAvailable = false;
            }

            //same time
            else if (trippickuptime.compareTo(cur_trippicuptime) == 0 && tripendtime.compareTo(cur_tripendtime) == 0){
                Log.d("Checking", "Time is Crashing - 2");
                isAvailable = false;
            }

            //trip stuck at the middle
            else if (trippickuptime.after(cur_trippicuptime) && tripendtime.before(cur_tripendtime)){
                Log.d("Checking", "Time is Crashing - 3");
                isAvailable = false;
            }

            else if (trippickuptime.before(cur_tripendtime) && tripendtime.after(cur_tripendtime)){
                Log.d("Checking", "Time is Crashing - 4");
                isAvailable = false;
            }

            else if (tripendtime.compareTo(cur_trippicuptime) == 0){
                Log.d("Checking", "Previous End Trip same as cur trip pick up");
            }

            else if (trippickuptime.compareTo(cur_trippicuptime) == 0) {
                Log.d("Checking", "Current End Trip same as next trip pick up");
            }
        }
        makeBooking();
    };

    public void checkabletopickup_nexttrip(String pickupTime , String startpoint){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            cal.setTime(sdf.parse(pickupTime));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }

        LatLng nextStart = getLatLngFromAddress(startpoint);
        LatLng thisTripend;
        if (reservation.getOneWay()) {
            thisTripend = getLatLngFromAddress(reservation.getDestination());
        }
        else
            thisTripend = getLatLngFromAddress(reservation.getOrigin());

        String url = getDirectionsUrl(thisTripend.latitude, thisTripend.longitude, nextStart.latitude, nextStart.longitude);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);

        int hour = 0;
        int min = 0;

        String[] splited = duration.split("\\s+");

        if (duration.contains("hour")) {
            int i = duration.indexOf(' ');
            hour = Integer.parseInt(splited[0]);
            min = Integer.parseInt(splited[2]);
        } else {
            min = Integer.parseInt(splited[0]);
        }

        if (min >= 30) {
            hour = hour + 1;
            min = 0;
        } else
            min = 30;

        if (hour > 0) {
            cal.add(Calendar.HOUR, -hour);
        }

        if (min > 0) {
            cal.add(Calendar.MINUTE, -min);
        }

        String thisTripendTime= reservation.getEndTime();

        Date endTime = null;
        DateFormat df = new SimpleDateFormat("HH:mm");
        try {
            endTime = df.parse(thisTripendTime);// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (cal.getTime().before(endTime)) {
            isAvailable = false;
        }
    }

    public void checkabletopickup_thistrip(String returnTime, String endpoint){

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            cal.setTime(sdf.parse(returnTime));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }

        LatLng previousEnd = getLatLngFromAddress(endpoint);

        LatLng thisTrippickup = getLatLngFromAddress(reservation.getOrigin());


        String url = getDirectionsUrl(previousEnd.latitude, previousEnd.longitude, thisTrippickup.latitude, thisTrippickup.longitude);
        DownloadTask downloadTask = new DownloadTask();

        downloadTask.execute(url);

        int hour = 0;
        int min = 0;

        String[] splited = duration.split("\\s+");

        if (duration.contains("hour")) {
            int i = duration.indexOf(' ');
            hour = Integer.parseInt(splited[0]);
            min = Integer.parseInt(splited[2]);
        } else {
            min = Integer.parseInt(splited[0]);
        }

        if (min >= 30) {
            hour = hour + 1;
            min = 0;
        } else
            min = 30;

        if (hour > 0) {
            cal.add(Calendar.HOUR, hour);
        }

        if (min > 0) {
            cal.add(Calendar.MINUTE, min);
        }

        String thisTripPickUpTime = reservation.getStartTime();

        Date pickuptime = null;
        DateFormat df = new SimpleDateFormat("HH:mm");
        try {
            pickuptime = df.parse(thisTripPickUpTime);// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (cal.getTime().after(pickuptime)){
            Log.d("Checking", "Time is Crashing - second condition");
            isAvailable = false;
        }
    }

    public LatLng getLatLngFromAddress(String strAddress){

        Log.d("Checking", "getLatLngFromAddress strAddress" + strAddress);
        Geocoder coder = new Geocoder(getContext());
        List<Address> address;
        LatLng latLng = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return latLng;
    }

    public void showProgressBar(){
        progressDialog = ProgressDialog.show(getContext(), "", "Checking Time Slot", true);
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animShow){

        }
        else if (animation == animHide){

        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("RequestCode", "Code " + requestCode);
        switch (requestCode){
            case 1:{
                if (resultCode == Activity.RESULT_OK){
                    Bundle bundle = data.getExtras();
                    String address = bundle.getString("placeName");
                    pickup_FullAddress = bundle.getString("FinalAddress");

                    reservation.setbDetails_Origin(pickup_FullAddress);
                    List<Address> addresses = bundle.getParcelableArrayList("Address");
                    bet_Origin.setText(address);
                    onSearchPickupLocation(addresses);
                }
                break;
            }
            case 2: {
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String address = bundle.getString("placeName");
                    dropoff_FullAdress = bundle.getString("FinalAddress");

                    reservation.setbDetails_Destination(dropoff_FullAdress);
                    List<Address> addresses = bundle.getParcelableArrayList("Address");
                    bet_Destination.setText(address);
                    onSearchDestination(addresses);
                }
                break;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);

        buildGoogleApiClient();

        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(this,"onConnected",Toast.LENGTH_SHORT).show();
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null && status.equalsIgnoreCase("Add")) {
            //place marker at current position
            mMap.clear();
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.cur_marker));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            curMarker = mMap.addMarker(markerOptions);
        }
        else if (status.equalsIgnoreCase("Edit")){
            LatLng origin = getLatLngFromAddress(reservation.getOrigin());
            LatLng dest = getLatLngFromAddress(reservation.getDestination());

            MarkerOptions pickupmarker = new MarkerOptions()
                    .position(origin)
                    .title("Pick Up ?")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pickupmarker));

            pickupMarker  = mMap.addMarker(pickupmarker);

            MarkerOptions destmarker = new MarkerOptions()
                    .position(dest)
                    .title("Going Here ?")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.location));

            destMarker  = mMap.addMarker(destmarker);

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            builder.include(destMarker.getPosition());
            builder.include(pickupMarker.getPosition());

            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 250);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(origin, 17));
            mMap.animateCamera(cu);
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location) {
        //remove previous current location marker and add new one at current position

        if (curMarker != null){
            curMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.cur_marker));
        curMarker = mMap.addMarker(markerOptions);

    }

    //Origin latitude, Origin Longtitude, Destination Latitude, Destionation Longtitude
    private String getDirectionsUrl(double mOriginLatitude, double mOriginLongtitude, double mDestinationLatitude, double mDestinationLongtitude){

        LatLng origin = new LatLng(mOriginLatitude, mOriginLongtitude);
        LatLng dest = new LatLng(mDestinationLatitude, mDestinationLongtitude);

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception download url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            if (mapLine != null && getRoute == true){
                mapLine.remove();
                mapLine = null;
            }

            if(result.size()<1){
                Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);


                    points.add(position);
                }


                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(Color.CYAN);
                lineOptions.geodesic(true);
            }

            //Drawing polyline in the Google Map for the i-th route
            if (getRoute)
                mapLine = mMap.addPolyline(lineOptions);

        }
    }

    //get latitude and longtitude from LocationFragment name
    public void onSearchDestination(List<Address> addressList) {

        if (destMarker != null) {
            destMarker.remove();
            destMarker = null;
        }

        Address address = addressList.get(0);
        double latitude = address.getLatitude();
        double longitude = address.getLongitude();
        LatLng latLng = new LatLng(latitude,longitude);

        MarkerOptions marker = new MarkerOptions()
                .position(latLng)
                .title("Going Here ?")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location));

        destMarker  = mMap.addMarker(marker);

        if (destMarker != null && pickupMarker != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            builder.include(destMarker.getPosition());
            builder.include(pickupMarker.getPosition());

            collapseView();

            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 250);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            mMap.animateCamera(cu);

            LatLng pickup = pickupMarker.getPosition();
            LatLng dest = destMarker.getPosition();

            curMarker.remove();

            String url = getDirectionsUrl(pickup.latitude, pickup.longitude, dest.latitude, dest.longitude);
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);
        }
        else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }
    }

    public void onSearchPickupLocation(List<Address> addressList) {

        if (pickupMarker != null) {
            pickupMarker.remove();
            pickupMarker = null;
        }

        Address address = addressList.get(0);
        double latitude = address.getLatitude();
        double longitude = address.getLongitude();
        LatLng latLng = new LatLng(latitude,longitude);

        MarkerOptions marker = new MarkerOptions()
                .position(latLng)
                .title("Pick Up?")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pickupmarker));

        pickupMarker  = mMap.addMarker(marker);

        if (destMarker != null && pickupMarker != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            builder.include(destMarker.getPosition());
            builder.include(pickupMarker.getPosition());

            collapseView();

            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 250);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            mMap.animateCamera(cu);

            LatLng pickup = pickupMarker.getPosition();
            LatLng dest = destMarker.getPosition();

            curMarker.remove();

            String url = getDirectionsUrl(pickup.latitude, pickup.longitude, dest.latitude, dest.longitude);
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);
        }
        else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onBookingConfirmed();
    }
}
