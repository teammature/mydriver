package fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.google.android.gms.location.LocationListener;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mydriver.DirectionsJSONParser;
import com.example.user.mydriver.GPSTracker;
import com.example.user.mydriver.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class PickUpFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private OnFragmentInteractionListener mListener;
    private List<String> retList;
    private String str_Origin, str_Dest;
    private List<Address> origin;
    private List<Address> destination;
    private GoogleMap gMap;
    private GoogleApiClient mGoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private FloatingActionButton call;
    private String str_lat, str_long;
    private TextView tv_Origin, tv_Destination;
    private ProgressDialog pd;

    public PickUpFragment(List<String> list) {
        retList = list;
        str_Origin = retList.get(3);
        str_Dest = retList.get(4);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pick_up, container, false);

        call = (FloatingActionButton) view.findViewById(R.id.pu_btnCall);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity()).setTitle("Call")
                        .setMessage("Call " + retList.get(0) + " now?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
                                query.whereContains("username", "dj");

                                query.findInBackground(new FindCallback<ParseObject>() {
                                    @Override
                                    public void done(List<ParseObject> list, ParseException e) {
                                        if (e != null) {
                                            Log.e("Call Intent", "Unable to retrieve phone number.");
                                        } else {
                                            String phone = list.get(0).getString("phone");
                                            //Make call intent here
                                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                            callIntent.setData(Uri.parse("tel:" + phone));
                                            startActivity(callIntent);
                                        }
                                    }
                                });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){

        super.onActivityCreated(savedInstanceState);

        //Connect to Google Api Client to get driver's current location
        mGoogleApiClient.connect();

        final Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            origin = geocoder.getFromLocationName(str_Origin, 1);
            destination = geocoder.getFromLocationName(str_Dest, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        tv_Origin = (TextView) getActivity().findViewById(R.id.pu_tvOriginAddress);
        tv_Origin.setText(str_Origin);
        tv_Destination = (TextView) getActivity().findViewById(R.id.pu_tvDestinationAddress);
        tv_Destination.setText(str_Dest);

        GPSTracker gpsTracker = new GPSTracker(getActivity().getBaseContext());
        double latitude = 0, longitude = 0;

        //Set current location
        if(gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
        }

        final LatLng coordinates = new LatLng(latitude, longitude);

        //Generate map view
        final MapView mv = (MapView) getActivity().findViewById(R.id.puMap);
        mv.onCreate(savedInstanceState);
        mv.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                if (googleMap != null) { // Map is ready
                    gMap = googleMap;
                    gMap.getUiSettings().setZoomControlsEnabled(true);

                    gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15));

                    List<Address> pickup = null, dropoff = null;

                    final Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        pickup = geocoder.getFromLocationName(str_Origin, 1);
                        dropoff = geocoder.getFromLocationName(str_Dest, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    onSearchLocation(pickup, dropoff);
                    mv.onResume();

                    Log.i("PickUp Map: ", "Map loaded");
                } else {
                    Log.i("PickUp Map: ", "Error loading map");
                }
            }
        });

        final Button btn_start = (Button) getActivity().findViewById(R.id.pu_btnStart);
        btn_start.setTag(0);

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int status = (Integer) v.getTag();

                if (status == 0) {
                    new AlertDialog.Builder(getActivity()).setMessage("Pick up passenger now?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse("geo:0,0?q=" + origin.get(0).getLatitude() + "," +
                                            origin.get(0).getLongitude()));
                                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                        startActivity(intent);
                                    }
                                    btn_start.setText("Arrive");
                                    btn_start.setTag(1);
                                    call.setVisibility(View.VISIBLE);
                                    mListener.onPickUpInteraction(1, retList.get(0));
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).show();

                } else if (status == 1) {
                    new AlertDialog.Builder(getActivity()).setMessage("Notify passenger?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setData(Uri.parse("geo:"+ origin.get(0).getLatitude() + "," +
                                                    origin.get(0).getLongitude() + "?q=" + destination.get(0).getLatitude() + "," +
                                                    destination.get(0).getLongitude()));
                                            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                                startActivity(intent);
                                            }
                                            btn_start.setText("Complete");
                                            btn_start.setTag(2);
                                            mListener.onPickUpInteraction(2, retList.get(0));
                                        }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).show();

                } else if (status == 2) {
                    new AlertDialog.Builder(getActivity()).setMessage("Complete trip?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");
                                    query.whereEqualTo("r_Passenger_name", retList.get(0));
                                    query.whereEqualTo("r_Date", retList.get(1));
                                    query.whereEqualTo("r_StartTime", retList.get(2));
                                    query.whereEqualTo("r_Origin", retList.get(3));
                                    query.whereEqualTo("r_Destination", retList.get(4));
                                    query.whereEqualTo("isCompleted", false);

                                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                                        @Override
                                        public void done(ParseObject parseObject, ParseException e) {
                                            if (parseObject != null) {
                                                parseObject.put("isCompleted", true);
                                                parseObject.saveInBackground();
                                                Log.d("PickUp_Trip", "Trip complete");
                                            }
                                        }
                                    });
                                    mListener.onPickUpInteraction(3, "null");
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).show();
                }
            }
        });
    }

    //Create origin and destination marker
    private void onSearchLocation(List<Address> orig, List<Address> dest) {
        double oLat, oLong, dLat, dLong;
        Address address = orig.get(0);
        oLat = address.getLatitude();
        oLong = address.getLongitude();
        LatLng latLng = new LatLng(oLat, oLong);

        gMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Pick up here")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pickupmarker)));

        address = dest.get(0);
        dLat = address.getLatitude();
        dLong = address.getLongitude();
        latLng = new LatLng(dLat, dLong);

        gMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Going here")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location)));

        //Draw direction line from origin to destination
        getDirectionsUrl(oLat, oLong, dLat, dLong);
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception download url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(15 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            str_lat = String.valueOf(mLastLocation.getLatitude());
            str_long = String.valueOf(mLastLocation.getLongitude());
            Log.i("onConnected:", str_lat + ", " + str_long);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("Error", "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()){
            try{
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            }
            catch (IntentSender.SendIntentException e){
                e.printStackTrace();
            }
        }
        else{
            Log.i("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= ProgressDialog.show(getActivity(), "Calculating route", "Please Wait....");
        }

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
            pd.dismiss();
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            String distance, duration;
            Polyline mapLine;
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            if(result.size()<1){
                Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }


                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(Color.CYAN);
                lineOptions.geodesic(true);
            }

            //Drawing polyline in the Google Map for the i-th route
            mapLine = gMap.addPolyline(lineOptions);
        }
    }

    //Origin latitude, Origin Longitude, Destination Latitude, Destination Longitude
    private void getDirectionsUrl(double mOriginLatitude, double mOriginLongtitude, double mDestinationLatitude, double mDestinationLongtitude){

        LatLng origin = new LatLng(mOriginLatitude, mOriginLongtitude);
        LatLng dest = new LatLng(mDestinationLatitude, mDestinationLongtitude);

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        DownloadTask downloadTask = new DownloadTask();

        downloadTask.execute(url);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("onLocChanged:", String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));
    }

    public interface OnFragmentInteractionListener {
        void onPickUpInteraction(int flag, String username);
    }
}
