package fragment;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BookingDetails_Fragment extends Fragment {

    ParseUser currentUser = ParseUser.getCurrentUser();
    private Reservation reservation;
    private String status;
    private EditText bet_Date, bet_PickUpTime, bet_DropOffTime, bet_Note;
    private String bDate, bPickUpTime, bDropOffTime, bNote;
    private Boolean isDateEmpty = true, isPickUpTimeEmpty = true, isDropOffTimeEmpty = false;
    private Boolean timeValidation = true, dateValidation = true, isEqual = false;
    private TextView tv_msg_Date, tv_msg_PickUpTime, tv_msg_DropOffTime;
    private Button btn_continue, btn_oneWay, btn_Return;
    private boolean isOneWay = true;

    public BookingDetails_Fragment(String s, Reservation r) {
        // Required empty public constructor
        this.reservation = r;
        this.status = s;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_details_, container, false);

        MainActivity.toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        MainActivity.activity.setSupportActionBar(MainActivity.toolbar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceStated) {
        super.onActivityCreated(savedInstanceStated);

        btn_continue = (Button)getActivity().findViewById(R.id.btn_continue);
        btn_oneWay = (Button)getActivity().findViewById(R.id.btn_oneWay);
        btn_Return = (Button)getActivity().findViewById(R.id.btn_return);

        tv_msg_Date = (TextView)getActivity().findViewById(R.id.msg_emptydate);
        tv_msg_PickUpTime = (TextView)getActivity().findViewById(R.id.msg_emptypickupTime);
        tv_msg_DropOffTime = (TextView)getActivity().findViewById(R.id.msg_emptyreturnTime);

        bet_Date = (EditText)getActivity().findViewById(R.id.bet_date);
        bet_PickUpTime = (EditText)getActivity().findViewById(R.id.bet_pickupTime);
        bet_DropOffTime= (EditText)getActivity().findViewById(R.id.bet_dropoffTime);
        bet_DropOffTime.setEnabled(false);
        bet_Note = (EditText)getActivity().findViewById(R.id.bet_Note);

        if (status.equalsIgnoreCase("Edit")){
            bet_Date.setText(reservation.getDate());
            bet_PickUpTime.setText(reservation.getStartTime());
            if (reservation.getOneWay()) {
                isOneWay = true;
                btn_oneWay.setBackgroundResource(R.drawable.enabledbutton);
                btn_Return.setBackgroundResource(R.drawable.disabledbutton);
                bet_DropOffTime.setEnabled(false);
                bet_DropOffTime.setText("");
            }
            else{
                isOneWay = false;
                btn_oneWay.setBackgroundResource(R.drawable.disabledbutton);
                btn_Return.setBackgroundResource(R.drawable.enabledbutton);
                bet_DropOffTime.setEnabled(true);
                bet_DropOffTime.setText(reservation.getEndTime());
            }
            bet_Note.setText(reservation.getbDetails_Note());
        } else if (status.equalsIgnoreCase("Add")) {
            bet_Date.setText(reservation.getDate());
            bet_PickUpTime.setText(reservation.getStartTime());
        }

        //------------------------------call date picker--------------------------------------------
        bet_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                DatePickerFragment picker = new DatePickerFragment(1);
                picker.show(fm, "Date Picker");
            }
        });

        bet_Date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tv_msg_Date.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //------------------------------call pickup time picker-------------------------------------
        bet_PickUpTime.setOnClickListener(new View.OnClickListener() {
            Calendar c = Calendar.getInstance();
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timepicker = new TimePickerDialogFragment(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        bet_PickUpTime.setText(String.format("%02d:%02d", hourOfDay, minute));
                    }
                },c.get(Calendar.HOUR_OF_DAY),0,true);
                timepicker.setTitle("Pick Up Time");
                timepicker.show();
            }
        });

        bet_PickUpTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tv_msg_PickUpTime.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //------------------------------call dropoff time picker------------------------------------
        bet_DropOffTime.setOnClickListener(new View.OnClickListener() {
            Calendar c = Calendar.getInstance();
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timepicker = new TimePickerDialogFragment(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        bet_DropOffTime.setText(String.format("%02d:%02d", hourOfDay, minute));
                    }
                },c.get(Calendar.HOUR_OF_DAY),0,true);
                timepicker.setTitle("Drop Off Time");
                timepicker.show();
            }
        });

        bet_DropOffTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tv_msg_DropOffTime.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //---------------------------when One way Button clicked------------------------------------
        btn_oneWay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOneWay = true;
                btn_oneWay.setBackgroundResource(R.drawable.enabledbutton);
                btn_Return.setBackgroundResource(R.drawable.disabledbutton);
                bet_DropOffTime.setEnabled(false);
                bet_DropOffTime.setText("");
            }
        });

        //----------------------------when return Button clicked------------------------------------
        btn_Return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOneWay = false;
                btn_oneWay.setBackgroundResource(R.drawable.disabledbutton);
                btn_Return.setBackgroundResource(R.drawable.enabledbutton);
                bet_DropOffTime.setEnabled(true);
            }
        });

        //----------------------------when continue button clicked----------------------------------
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bet_Date.getText().toString().trim().length() == 0){
                    tv_msg_Date.setVisibility(View.VISIBLE);
                    isDateEmpty = true;
                }
                else{
                    isDateEmpty = false;
                }

                if (bet_PickUpTime.getText().toString().trim().length() == 0){
                    tv_msg_PickUpTime.setVisibility(View.VISIBLE);
                    isPickUpTimeEmpty = true;
                }
                else{
                    isPickUpTimeEmpty = false;
                }

                if (!isOneWay && bet_DropOffTime.getText().toString().trim().length() == 0){

                    tv_msg_DropOffTime.setVisibility(View.VISIBLE);
                    isDropOffTimeEmpty = true;
                }
                else{
                    isDropOffTimeEmpty = false;
                }

                try{

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Date date1 = sdf.parse(bet_Date.getText().toString());
                    Date date2 = new Date();

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date2);
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);


                    if(date1.before(calendar.getTime())){
                        dateValidation = false;
                    }

                    else if(date1.equals(calendar.getTime())){
                        dateValidation = true;
                    }
                    else {
                        dateValidation = true;
                    }


                }catch(ParseException ex){
                    ex.printStackTrace();
                }


                //pick up date is past date
                if (!dateValidation){
                    Toast.makeText(getActivity(), "There seems to be a problem with the Time / Date selected.\n" +
                            "Please ensure the Time / Date selected is for Today or Future Dates.", Toast.LENGTH_SHORT).show();
                }

                if(!isOneWay){
                    SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
                    Date pickup = null, dropoff = null;

                    try {
                        pickup = parser.parse(bet_PickUpTime.getText().toString());
                        dropoff = parser.parse(bet_DropOffTime.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (pickup.after(dropoff) || pickup.equals(dropoff)){
                           timeValidation = false;
                    }
                    else{
                        timeValidation = true;
                    }
                }
                else{
                    timeValidation = true;
                }

                //pick up time is after or equal drop off
                if (!timeValidation){
                    Toast.makeText(getActivity(), "There seems to be a problem with the timing selected.\n" +
                            "Please ensure the PickUp time is before the DropOff time.", Toast.LENGTH_SHORT).show();
                }


                if (!isDateEmpty && !isPickUpTimeEmpty && !isDropOffTimeEmpty && dateValidation && timeValidation) {
                    reservation.setbDetails_isOneWay(isOneWay);
                    reservation.setbDetails_Date(bet_Date.getText().toString());
                    reservation.setbDetails_StartTime(bet_PickUpTime.getText().toString());
                    reservation.setbDetails_EndTime(bet_DropOffTime.getText().toString());
                    reservation.setbDetails_Note(bet_Note.getText().toString());

                    //// TODO: 6/8/2016 check time slot take
                    temp_fragment getLocationfragment = new temp_fragment(reservation, status);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_Container, getLocationfragment, "Booking")
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
    }
}
