package fragment;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.mydriver.Adapter.PlacesAutoCompleteAdapter;
import com.example.user.mydriver.BackgroundTask.PlaceRetrieve_BgroundTask;
import com.example.user.mydriver.GPSTracker;
import com.example.user.mydriver.Adapter.PlaceArrayAdapter;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.example.user.mydriver.RecyclerItemClickListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class addPlaceFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private OnFragmentInteractionListener mListener;
    private GoogleApiClient fGoogleApiClient;
    private static EditText fmAtv;
    private ImageButton fmbtnCurLocation;
    private Button fmbtnDone;
    private String placeName;
    private int placeId;
    private double flatitude, flongitude;
    private String faddress;
    public static ListView place_listview;
    private PlaceArrayAdapter fPlaceArrayAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    GPSTracker gps;

    public addPlaceFragment(){};

    public addPlaceFragment(String name, int pid){
        placeName = name;
        placeId = pid;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_place, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        fmAtv = (EditText)getActivity().findViewById(R.id.fatv);
        fmbtnCurLocation = (ImageButton)getActivity().findViewById(R.id.fbtn_cur_location);
        fmbtnDone = (Button)getActivity().findViewById(R.id.fbtn_Done);

        fGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();

        mAutoCompleteAdapter =  new PlacesAutoCompleteAdapter(getContext(), R.layout.search_row,
                fGoogleApiClient, BOUNDS_MOUNTAIN_VIEW, null);

        mRecyclerView=(RecyclerView)getView().findViewById(R.id.apRecycleView);
        mLinearLayoutManager=new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);

        fmAtv.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("") && fGoogleApiClient.isConnected()) {
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                } else if (!fGoogleApiClient.isConnected()) {
                    Toast.makeText(getContext(), "API Not Connected", Toast.LENGTH_SHORT).show();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mAutoCompleteAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.i("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                         */

                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(fGoogleApiClient, placeId);
                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if (places.getCount() == 1) {
                                    //Do the things here on Click.....
                                    fmAtv.setText(places.get(0).getName());
                                } else {
                                    Toast.makeText(getActivity(), "Something Wrong", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        Log.i("TAG", "Clicked: " + item.description);
                        Log.i("TAG", "Called getPlaceById to get Place details for " + item.placeId);
                    }
                })
        );

        //user press current LocationFragment button to get the current LocationFragment
        fmbtnCurLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gps = new GPSTracker(getActivity().getBaseContext());

                if (gps.canGetLocation()) {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    flatitude = gps.getLatitude();
                    flongitude = gps.getLongitude();

                    updateBox(latitude, longitude);
                } else {
                    gps.showSettingAlert();
                }
            }
        });

        fmbtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fmAtv.getText().toString().equals("")) {
                    faddress = fmAtv.getText().toString();
                    LatLng latLng = getLocationFromAddress(getContext(), faddress);
                    Log.d("Add Place", "Got Latlng " + flatitude + " & " + flongitude);
                    flatitude = latLng.latitude;
                    flongitude = latLng.longitude;

                    mListener.onPlaceDetailsReady(placeName, flatitude, flongitude, faddress, placeId);
                } else
                    Toast.makeText(getActivity(), "No Address Entered", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = fPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(fGoogleApiClient, placeId);
        }
    };

    public LatLng getLocationFromAddress(Context context,String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    private void updateBox(double curLat, double curLong) {
        Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
        StringBuilder builder = new StringBuilder();
        try {
            List<Address> listaddress = geoCoder.getFromLocation(curLat, curLong, 1);
            int maxLines = listaddress.get(0).getMaxAddressLineIndex();
            for (int i = 0; i < maxLines; i++) {
                String addressStr = listaddress.get(0).getAddressLine(i);
                builder.append(addressStr);
                builder.append(" ");
            }

            String finalAddress = builder.toString(); //This is the complete address.
            faddress = finalAddress;
            fmAtv.setText(finalAddress);

        } catch (IOException e) {

        } catch (NullPointerException e) {

        }
    }

    public void onResume() {
        super.onResume();
        fGoogleApiClient.connect();
    }

    public void onPause() {
        super.onPause();
        if (fGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(fGoogleApiClient, this);
            fGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        //fPlaceArrayAdapter.setGoogleApiClient(fGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
        fPlaceArrayAdapter.setGoogleApiClient(null);
    }

    @Override
    public void onLocationChanged(Location location) {
        //
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(getActivity(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnaddPlaceFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onPlaceDetailsReady(String name, double lat, double lng, String address, int placeid);
    }
}
