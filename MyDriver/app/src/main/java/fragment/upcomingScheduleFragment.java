package fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.mydriver.Adapter.BDRecyclerAdapter;
import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.example.user.mydriver.RecyclerItemClickListener;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class upcomingScheduleFragment extends Fragment{

    private OnFragmentInteractionListener mListener;

    private Dialog progressDialog;

    private Date ComparedDate;
    private RecyclerView rv_manageList;
    private RecyclerView.LayoutManager h_LinearLayoutManager;
    private List<Reservation> reservations = new ArrayList<Reservation>();
    private BDRecyclerAdapter adapter = new BDRecyclerAdapter();

    public upcomingScheduleFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_schedule, container, false);

        MainActivity.toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        MainActivity.activity.setSupportActionBar(MainActivity.toolbar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);


        rv_manageList = (RecyclerView)getActivity().findViewById(R.id.upcomingschedule_recyclerview);
        h_LinearLayoutManager = new LinearLayoutManager(getContext());
        rv_manageList.setLayoutManager(h_LinearLayoutManager);
        adapter = new BDRecyclerAdapter(reservations);
        rv_manageList.setAdapter(adapter);

        //-----------get current date
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 2);

        String formattedDate = df.format(calendar.getTime());

        try {
            ComparedDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ParseUser currentUser = ParseUser.getCurrentUser();

        Log.d("Today Date", "Today Date = " + ComparedDate);

        //------------- query incoming reservation
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");

        query.whereGreaterThanOrEqualTo("Date", ComparedDate);
        query.whereEqualTo("r_Passenger_name", currentUser.getUsername());
        query.whereEqualTo("isCompleted", false);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, com.parse.ParseException e) {
                if (e == null) {
                    Log.d("Retrieving List", "List Size " + list.size());
                    setRecyclerCardView(list);
                    dismissProgressBar();
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


        rv_manageList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final Reservation reservation = reservations.get(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Edit Booking");
                builder.setMessage("Do you want to edit this booking ?");

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onEditBookingPressed("Edit", reservation);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        }));
    }

    public void setRecyclerCardView(List<ParseObject> list){

        clearData();

        for (int i = 0; i < list.size(); i++) {
            Date r_date = list.get(i).getDate("Date");

            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(r_date);
            calendar.add(Calendar.DATE, -1);

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String date = df.format(calendar.getTime());

            String objectID = list.get(i).getObjectId();
            String r_estimatepickup = list.get(i).getString("r_EstimatedPickUpTime");
            String r_startTime = list.get(i).getString("r_StartTime");
            String r_endTime = list.get(i).getString("r_EndTime");
            String r_estimateendtrip = list.get(i).getString("r_EstimatedEndTripTime");
            String r_originName = list.get(i).getString("r_OriginName");
            String r_originAddress = list.get(i).getString("r_Origin");
            String r_destinationName = list.get(i).getString("r_DestinationName");
            String r_desitnationAddress = list.get(i).getString("r_Destination");
            boolean r_isOneWay = list.get(i).getBoolean("r_isOneWay");
            String r_note = list.get(i).getString("r_Note");

            Log.d("Future Booking", "Object ID = " + objectID);
            Reservation r = new Reservation(objectID, date, r_estimatepickup, r_startTime, r_endTime, r_estimateendtrip,
                    r_originName, r_originAddress, r_destinationName, r_desitnationAddress, r_isOneWay, r_note);
            reservations.add(r);
            adapter.notifyItemInserted(reservations.size());
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void showProgressBar(){
        progressDialog = ProgressDialog.show(getContext(), "", "Retrieving Booking", true);
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public interface OnFragmentInteractionListener {
        void onEditBookingPressed(String status, Reservation reservation);
    }

    public void clearData() {
        int size = reservations.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                reservations.remove(0);
            }
            adapter.notifyItemRangeRemoved(0, size);
        }
    }
}
