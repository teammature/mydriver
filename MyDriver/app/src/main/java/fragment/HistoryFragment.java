package fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.user.mydriver.Adapter.BDRecyclerAdapter;
import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.example.user.mydriver.RecyclerItemClickListener;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HistoryFragment extends Fragment {

    //private OnFragmentInteractionListener mListener;
    private RecyclerView rv_historyList;
    private RecyclerView.LayoutManager h_LinearLayoutManager;
    private final List<Reservation> reservations = new ArrayList<Reservation>();
    private BDRecyclerAdapter adapter = new BDRecyclerAdapter();
    private Date Todaydate;
    private Dialog progressDialog;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        MainActivity.toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        MainActivity.activity.setSupportActionBar(MainActivity.toolbar);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showProgressBar();

        //-----------get current date
        Calendar calendar = Calendar.getInstance();

        rv_historyList = (RecyclerView) getActivity().findViewById(R.id.history_recyclerview);
        h_LinearLayoutManager = new LinearLayoutManager(getContext());
        rv_historyList.setLayoutManager(h_LinearLayoutManager);

        adapter = new BDRecyclerAdapter(reservations);
        rv_historyList.setAdapter(adapter);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(calendar.getTime());

        try {
            Todaydate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ParseUser currentUser = ParseUser.getCurrentUser();

        //----------------- query history reservation
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");
        query.whereEqualTo("r_Passenger_name", currentUser.getUsername());
        query.whereEqualTo("isCompleted", true);
        query.whereLessThanOrEqualTo("Date", Todaydate);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, com.parse.ParseException e) {
                if (e == null) {
                    setRecyclerCardView(list);
                    dismissProgressBar();
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

        rv_historyList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final Reservation reservation = reservations.get(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("View History");
                builder.setMessage("Do you want to view the details of this reservation ?");

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ViewHistoryFragment viewhistory = new ViewHistoryFragment(reservation);
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment_Container, viewhistory, "View History")
                                .addToBackStack(null)
                                .commit();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        }));
    }

    public void setRecyclerCardView(List<ParseObject> list) {

        Log.d("Checking", "Data Retrieved : " + list.size());

        clearData();

        for (int i = 0; i < list.size(); i++) {
            Date r_date = list.get(i).getDate("Date");

            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(r_date);
            calendar.add(Calendar.DATE, -1);

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String date = df.format(calendar.getTime());

            String r_startTime = list.get(i).getString("r_StartTime");
            String r_originName = list.get(i).getString("r_OriginName");
            String r_originAddress = list.get(i).getString("r_Origin");
            String r_destinationName = list.get(i).getString("r_DestinationName");
            String r_desitnationAddress = list.get(i).getString("r_Destination");
            boolean r_isOneWay = list.get(i).getBoolean("r_isOneWay");
            String r_endTime = list.get(i).getString("r_EndTime");

            Log.d("Retrieving Booking", "Date " + date);
            Reservation reservation = new Reservation(date, r_startTime,
                    r_originName, r_originAddress,  r_destinationName, r_desitnationAddress, r_isOneWay, r_endTime);
            reservations.add(reservation);
            adapter.notifyItemInserted(reservations.size());
        }
    }

    public void showProgressBar(){
        progressDialog = ProgressDialog.show(getContext(), "", "Retrieving History", true);
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void clearData() {
        int size = reservations.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                reservations.remove(0);
            }
            adapter.notifyItemRangeRemoved(0, size);
        }
    }
}
