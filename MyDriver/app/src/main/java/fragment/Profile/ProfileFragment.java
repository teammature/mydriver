package fragment.Profile;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mydriver.BackgroundTask.PlaceRetrieve_BgroundTask;
import com.example.user.mydriver.Data.Place;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.example.user.mydriver.dbFavouritePlaceOperations;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import fragment.addPlaceFragment;
import fragment.viewAllPlaceFragment;

public class ProfileFragment extends Fragment {
    private TextView btn_seeMore;
    private EditText p_username, p_phoneNo, p_Email;
    private Spinner sp_operatorNo;
    private static final String[]operatorNo = {"010", "011", "012", "013", "014", "015", "016", "017", "018", "019"};
    private String name = "";
    private ListView favouritePlace_listview;
    ParseUser currentUser = ParseUser.getCurrentUser();

    public ProfileFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit: {

                ProfileEditableFragment peFragment = new ProfileEditableFragment();
                this.getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_Container, peFragment, "Edit Profile")
                        .addToBackStack(null)
                        .commit();

                MainActivity.toolbar.setTitle("Edit Profile");
                return true;
            }

            case R.id.action_ResetPassword: {
                // TODO: 4/28/2016 reset password
                DialogFragment newFragment = changePwdFragment
                        .newInstance("Change Password");
                newFragment.show(getActivity().getFragmentManager(), "dialog");

                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        String operatorno;

        sp_operatorNo = (Spinner)getActivity().findViewById(R.id.spinner_operatorNo);
        p_username = (EditText)getActivity().findViewById(R.id.p_etuserName);
        p_phoneNo = (EditText)getActivity().findViewById(R.id.p_etphoneNo);
        p_Email = (EditText)getActivity().findViewById(R.id.p_etEmail);

        p_username.setText(currentUser.getString("username"));
        p_username.setEnabled(false);

        p_phoneNo.setText(currentUser.getString("phone").substring(3));
        p_phoneNo.setEnabled(false);

        p_Email.setText(currentUser.getString("email"));
        p_Email.setEnabled(false);

        //Retriving the Profile Picture from Parse
        ParseFile imageFile = (ParseFile) currentUser.get("profilePicture");

        if (imageFile != null) {
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, com.parse.ParseException e) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    ImageView profilePicture = (ImageView) getActivity().findViewById(R.id.profile_pic);
                    profilePicture.setImageBitmap(bitmap);
                }
            });
        }

        //get operator last digit
        operatorno = currentUser.getString("phone").substring(2,3);

        //set up spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_textstyle, operatorNo);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_operatorNo.setAdapter(adapter);
        sp_operatorNo.setSelection(Integer.parseInt(operatorno));
        sp_operatorNo.setEnabled(false);
        sp_operatorNo.setClickable(false);

        //------------------------------------Favourite place---------------------------------------

        favouritePlace_listview = (ListView)getActivity().findViewById(R.id.lv_favouritePlace);
        btn_seeMore = (TextView)getActivity().findViewById(R.id.tv_seeMore);

        PlaceRetrieve_BgroundTask bgTask = new PlaceRetrieve_BgroundTask(getContext());
        bgTask.execute("show_place");

        favouritePlace_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Place place = (Place) favouritePlace_listview.getItemAtPosition(position);

                Log.d("Place name ", "name -> " + place.getPlace_Name());

                if (place.getPlace_Name().contains("Add Home")) {
                    addPlaceFragment addPlaceFragment = new addPlaceFragment("Home", 0);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_Container, addPlaceFragment, "AddHome")
                            .addToBackStack(null)
                            .commit();
                }
                else if (place.getPlace_Name().contains("Add Work")) {
                    addPlaceFragment addPlaceFragment = new addPlaceFragment("Work", 0);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_Container, addPlaceFragment, "AddWork")
                            .addToBackStack(null)
                            .commit();
                }
                else if (place.getPlace_Name().equals("Add Favourite")) {
                    addPlaceFragment addPlaceFragment = new addPlaceFragment("Favourite", 0);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_Container, addPlaceFragment, "AddFavourite")
                            .addToBackStack(null)
                            .commit();
                }
                else {
                    //nothing
                }
            }
        });

        btn_seeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAllPlaceFragment viewPlacesFragment = new viewAllPlaceFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_Container, viewPlacesFragment, "View Favourite Place")
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        MainActivity.activity.setSupportActionBar(mToolbar);

        return view;
    }

    public void updateDetails(String name, String email, String phoneNo, String operatorNo){

        sp_operatorNo = (Spinner)getActivity().findViewById(R.id.spinner_operatorNo);
        p_username = (EditText)getActivity().findViewById(R.id.p_etuserName);
        p_phoneNo = (EditText)getActivity().findViewById(R.id.p_etphoneNo);
        p_Email = (EditText)getActivity().findViewById(R.id.p_etEmail);

        p_username.setText(name);

        p_phoneNo.setText(phoneNo);

        p_Email.setText(email);
    }
}

