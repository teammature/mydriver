package fragment.Profile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.mydriver.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

/**
 * Created by Owner on 5/12/2016.
 */
public class resetPwdFragment extends DialogFragment{

    private EditText et_email;
    private String title;
    private Context ctx;
    ParseUser currentUser = ParseUser.getCurrentUser();

    public resetPwdFragment(String title, Context context) {
        this.title = title;
        ctx = context;
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean isRequested = false;
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_resetpwd, null);

        et_email = (EditText)view.findViewById(R.id.et_resetpwd_email);

        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setView(view)
                .setPositiveButton("Submit",
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                int whichButton) {
                                String email = et_email.getText().toString();

                                if (et_email.length() > 0) {
                                    try{
                                        ParseUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (e == null) {
                                                    Toast.makeText(ctx, "An Email has been sent to the email address you provided! \n" +
                                                            "Follow the procedure to reset your Password.", Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(ctx, "Unfortunately, a problem was encountered.\nDetails:" + e, Toast.LENGTH_LONG).show();
                                                }

                                            }
                                        });
                                    } catch(Exception e){
                                        Toast.makeText(ctx,"Error" + e,Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
    }
}
