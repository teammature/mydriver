package fragment.Profile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.mydriver.R;
import com.parse.ParseUser;

public class changePwdFragment extends DialogFragment {

    private EditText et_pwd, et_cfmpwd;
    ParseUser currentUser = ParseUser.getCurrentUser();

    public changePwdFragment() {
        // Required empty public constructor
    }

    public static changePwdFragment newInstance(String title) {
        changePwdFragment frag = new changePwdFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_changepwd, null);

        et_pwd = (EditText)view.findViewById(R.id.et_newpwd);
        et_cfmpwd = (EditText)view.findViewById(R.id.et_confirmpwd);

        String title = getArguments().getString("title");

        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setView(view)
                .setPositiveButton("Submit",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                String newPwd = et_pwd.getText().toString();
                                String cfmPwd = et_cfmpwd.getText().toString();

                                if (newPwd.equals(cfmPwd)) {
                                    currentUser.setPassword(newPwd);
                                    currentUser.saveInBackground();
                                    Toast.makeText(getActivity(), "Password successfully changed.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), "Password change Failed!\nConfirmation Password" +
                                            "does not match the New Password.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                dialog.dismiss();
                            }
                        }).create();
    }
}
