package fragment.Profile;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class ProfileEditableFragment extends Fragment {

    private EditText p_username, p_phoneNo, p_Email;
    private OnFragmentInteractionListener mListener;
    private static final String[]arr_operatorNo = {"010", "011", "012", "013", "014", "015", "016", "017", "018", "019"};
    private Spinner spinner_operatorNo;
    private Button btn_save;
    private Dialog progressDialog;
    private TextView tvusername, tvemail, tvphoneno;
    private NavigationView navigationView;
    private String Name, phoneNo, Email, operatorNo;
    private Boolean isNameEmpty = true, isEmailEmpty = true, isPhoneNoEmpty = true;

    //Profile Picture
    private Button edit_profilePic;
    private static final int SELECT_PHOTO = 1;
    private ImageView profilePicture;
    public Bitmap selectedImagePath;
    ParseUser currentUser = ParseUser.getCurrentUser();
    private Boolean isDone = false;


    public ProfileEditableFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == Activity.RESULT_OK) try {
                    final Uri imageUri = imageReturnedIntent.getData();
                    final InputStream imageStream = getContext().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImagePath = BitmapFactory.decodeStream(imageStream);

                    //Begins to upload the image to parse

                    // Convert it to byte
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] image = stream.toByteArray();

                    // Create the ParseFile
                    ParseFile file = new ParseFile("ProfilePicture.png", image);
                    // Upload the image into Parse Cloud
                    file.saveInBackground();

                    // Create a column named "ImageFile" and insert the image
                    currentUser.put("profilePicture", file);

                    // Create the class and the columns
                    currentUser.saveInBackground();

                    //Set the selected Image as the Profile Picture
                    ImageView newPic = (ImageView)getView().findViewById(R.id.profile_pic);
                    newPic.setImageBitmap(selectedImagePath);

                    //Retrieving the Profile Picture from Parse
                    ParseFile imageFile = (ParseFile) currentUser.get("profilePicture");
                    imageFile.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, com.parse.ParseException e) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                            ImageView profilePicture = (ImageView) getActivity().findViewById(R.id.profile_pic);
                            profilePicture.setImageBitmap(bitmap);
                        }
                    });

                    navigationView = (NavigationView)getActivity().findViewById(R.id.navigation_view);

                    final View header = navigationView.getHeaderView(0);

                    ParseFile imageFile2 = (ParseFile) currentUser.get("profilePicture");
                    imageFile2.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, com.parse.ParseException e) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                            ImageView profilePicture = (ImageView) header.findViewById(R.id.profile_pic);
                            profilePicture.setImageBitmap(bitmap);
                        }
                    });

                    Toast.makeText(getActivity(), "Profile Picture Successfully changed.", Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        String operatorno;

        final ParseUser currentUser = ParseUser.getCurrentUser();

        spinner_operatorNo = (Spinner)getView().findViewById(R.id.spr_OpNo);
        p_username = (EditText)getView().findViewById(R.id.p_etuserName);
        p_phoneNo = (EditText)getView().findViewById(R.id.p_etphoneNo);
        p_Email = (EditText)getView().findViewById(R.id.p_etEmail);
        btn_save = (Button)getView().findViewById(R.id.p_doneEdit);

        tvusername = (TextView)getActivity().findViewById(R.id.username);
        tvemail = (TextView)getActivity().findViewById(R.id.email);
        tvphoneno = (TextView)getActivity().findViewById(R.id.phonenumber);

        p_username.setText(currentUser.getString("username"));
        p_phoneNo.setText(currentUser.getString("phone").substring(3));
        p_Email.setText(currentUser.getString("email"));


        //Profile picture
        edit_profilePic = (Button)getActivity().findViewById(R.id.editProfileButton);

        //Retrieving the Profile Picture from Parse
        ParseFile imageFile = (ParseFile) currentUser.get("profilePicture");

        if (imageFile != null) {
            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, com.parse.ParseException e) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    ImageView profilePicture = (ImageView) getView().findViewById(R.id.profile_pic);
                    profilePicture.setImageBitmap(bitmap);
                }
            });
        }


        //get operator last digit
        operatorno = currentUser.getString("phone").substring(2,3);

        //set up spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, arr_operatorNo);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_operatorNo.setAdapter(adapter);
        spinner_operatorNo.setSelection(Integer.parseInt(operatorno));

        p_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvusername.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        p_phoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvemail.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        p_Email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvphoneno.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                                            Name = p_username.getText().toString();
                                            operatorNo = spinner_operatorNo.getSelectedItem().toString();
                                            phoneNo = p_phoneNo.getText().toString();
                                            Email = p_Email.getText().toString();


                                            //Validation - name
                                            if (Name.trim().length() == 0)
                                            {
                                                tvusername.setVisibility(View.VISIBLE);
                                                isNameEmpty = true;
                                            }

                                            else if (Name.length() < 2)
                                            {
                                                Toast.makeText(getActivity(),"Name length must be longer than 2 characters",Toast.LENGTH_LONG).show();
                                                isNameEmpty = true;
                                            }

                                            else{
                                                isNameEmpty = false;
                                            }

                                            //Validation - phone
                                            if (operatorNo.trim().length() == 0 || phoneNo.trim().length() == 0
                                                    || phoneNo.trim().length() > 8)
                                            {
                                                tvphoneno.setVisibility(View.VISIBLE);
                                                isPhoneNoEmpty = true;
                                            }
                                            else{
                                                isPhoneNoEmpty = false;
                                            }

                                            //Validation - email
                                            if (Email.trim().length() == 0)
                                            {
                                                tvemail.setVisibility(View.VISIBLE);
                                                isEmailEmpty = true;
                                            }
                                            else if (!Email.matches(emailPattern)){
                                                Toast.makeText(getActivity(),"A valid email is required",Toast.LENGTH_LONG).show();
                                                isEmailEmpty = true;
                                            }
                                            else{
                                                isEmailEmpty = false;
                                            }

                                            if (!isNameEmpty && !isPhoneNoEmpty && !isEmailEmpty) {
                                                showProgressBar();
                                                ParseUser currentUser = ParseUser.getCurrentUser();
                                                ParseQuery<ParseUser> query = ParseUser.getQuery();
                                                query.getInBackground(currentUser.getObjectId(), new GetCallback<ParseUser>() {
                                                    @Override
                                                    public void done(ParseUser parseUser, com.parse.ParseException e) {
                                                        parseUser.setUsername(p_username.getText().toString());
                                                        parseUser.setEmail(p_Email.getText().toString());
                                                        String phoneNo = spinner_operatorNo.getSelectedItem().toString() + p_phoneNo.getText().toString();
                                                        parseUser.put("phone", phoneNo);

                                                        // This will throw an exception, since the ParseUser is not authenticated
                                                        parseUser.saveInBackground();

                                                        dismissProgressBar();
                                                        Toast.makeText(getActivity(), "Profile Details successfully Updated.", Toast.LENGTH_SHORT).show();
                                                        mListener.onDoneClicked(p_username.getText().toString(),
                                                                spinner_operatorNo.getSelectedItem().toString(),
                                                                p_phoneNo.getText().toString(), p_Email.getText().toString());
                                                    }
                                                });
                                            }
                                        }
                                    }
        );

        edit_profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Change Picture");

                AlertDialog.Builder editPic = builder.setMessage("Would you like to change your current Profile Picture?")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //Photo Selection Screen
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                            }
                        })
                        ;

                AlertDialog editProfilePic = builder.create();

                editProfilePic.show();
            }
        });

    }

    public void showProgressBar(){
        progressDialog = ProgressDialog.show(getActivity(), "", "Updating Profile", true);
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_editable, container, false);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onDoneClicked(String name, String operatorno, String phoneNo, String email);
    }
}
