package fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.user.mydriver.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class RegisterFragment extends Fragment {

    private Button btnRegister;
    private Spinner sp_operatorNo;
    private static final String[]arr_operatorNo = {"010", "011", "012", "013", "014", "015", "016", "017", "018", "019"};
    private CheckBox cbx_psgr, cbx_driver;
    private TextView tvusername, tvpassword, tvemail, tvphoneno;
    private EditText et_Name, et_PhoneNo, et_Email, et_Password;
    private String Name, phoneNo, Email, Password, operatorNo;
    private Boolean isNameEmpty = true, isPasswordEmpty = true, isEmailEmpty = true, isPhoneNoEmpty = true;
    private Dialog progressDialog;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        sp_operatorNo = (Spinner)view.findViewById(R.id.r_spr_operatorNo);

        //set up spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, arr_operatorNo);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_operatorNo.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.logOut();

        btnRegister = (Button) getActivity().findViewById(R.id.btn_Register);
        cbx_psgr = (CheckBox)getActivity().findViewById(R.id.cb_psgr);
        cbx_driver = (CheckBox)getActivity().findViewById(R.id.cb_driver);

        //Edit Text
        et_Name = (EditText) getActivity().findViewById(R.id.et_Name);
        sp_operatorNo = (Spinner)getActivity().findViewById(R.id.r_spr_operatorNo);
        et_PhoneNo = (EditText) getActivity().findViewById(R.id.et_phoneNo);
        et_Email = (EditText) getActivity().findViewById(R.id.et_Email);
        et_Password = (EditText) getActivity().findViewById(R.id.et_Password);

        tvusername = (TextView)getActivity().findViewById(R.id.username);
        tvpassword = (TextView)getActivity().findViewById(R.id.password);
        tvemail = (TextView)getActivity().findViewById(R.id.email);
        tvphoneno = (TextView)getActivity().findViewById(R.id.phonenumber);

        et_Name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvusername.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        et_Password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvpassword.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        et_Email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvemail.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        et_PhoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    tvphoneno.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        cbx_psgr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbx_driver.setChecked(false);
            }
        });

        cbx_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbx_psgr.setChecked(false);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //convert Edit Text to String
                Name = et_Name.getText().toString();
                operatorNo = sp_operatorNo.getSelectedItem().toString();
                phoneNo = et_PhoneNo.getText().toString();
                Email = et_Email.getText().toString();
                Password = et_Password.getText().toString();

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                //Validation - name
                if (Name.trim().length() == 0)
                {
                    tvusername.setVisibility(View.VISIBLE);
                    isNameEmpty = true;
                }

                else if (Name.length() < 2)
                {
                    Toast.makeText(getActivity(),"Name length must be longer than 2 characters!",Toast.LENGTH_LONG).show();
                    isNameEmpty = true;
                }

                else{
                    isNameEmpty = false;
                }

                //Validation - phone
                if (operatorNo.trim().length() == 0 || phoneNo.trim().length() == 0
                        || phoneNo.trim().length() > 8)
                {
                    tvphoneno.setVisibility(View.VISIBLE);
                    isPhoneNoEmpty = true;
                }
                else{
                    isPhoneNoEmpty = false;
                }

                //Validation - email
                if (Email.trim().length() == 0)
                {
                    tvemail.setVisibility(View.VISIBLE);
                    isEmailEmpty = true;
                }
                else if (!Email.matches(emailPattern)){
                    Toast.makeText(getActivity(),"A valid email address is required!",Toast.LENGTH_LONG).show();
                    isEmailEmpty = true;
                }
                else{
                    isEmailEmpty = false;
                }

                //Validation - password
                if (Password.trim().length() == 0)
                {
                    tvpassword.setVisibility(View.VISIBLE);
                    isPasswordEmpty = true;
                }
                else if (Password.length() < 6)
                {
                    Toast.makeText(getActivity(),"Password length must be 6 characters or longer!",Toast.LENGTH_LONG).show();
                    isPasswordEmpty = true;
                }
                else{
                    isPasswordEmpty = false;
                }

                if (!isNameEmpty && !isPhoneNoEmpty && !isPasswordEmpty && !isEmailEmpty) {
                    showProgressBar();
                    ParseUser user = new ParseUser();
                    user.setUsername(Name);
                    user.setPassword(Password);
                    user.setEmail(Email);
                    user.put("phone", operatorNo + phoneNo);
                    user.put("isPassenger", cbx_psgr.isChecked());

                    user.signUpInBackground(new SignUpCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                dismissProgressBar();
                                Toast.makeText(getActivity(), "Registration Successful.", Toast.LENGTH_SHORT).show();
                            } else {
                                dismissProgressBar();
                                Toast.makeText(getActivity(), "Registration Failed!\nDetails:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    public void showProgressBar(){
        progressDialog = ProgressDialog.show(getContext(), "", "Registering", true);
    }

    public void dismissProgressBar(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
