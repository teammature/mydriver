package fragment;

//pickup/dropoff location dialog fragment
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.mydriver.Adapter.PlacesAutoCompleteAdapter;
import com.example.user.mydriver.BackgroundTask.B_FPlaceRetrieve_BgroundTask;
import com.example.user.mydriver.Data.Place;
import com.example.user.mydriver.GPSTracker;
import com.example.user.mydriver.Adapter.PlaceArrayAdapter;
import com.example.user.mydriver.R;

import com.example.user.mydriver.RecyclerItemClickListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PDLocation_DialogFragment extends DialogFragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener{

    private GoogleApiClient PD_GoogleApiClient;
    private static EditText PD_mAtv;
    private Button PD_btnDone;
    private double PD_latitude, PD_longitude;
    private String PD_address;
    private PlaceArrayAdapter PD_PlaceArrayAdapter;
    public static ListView PD_place_listview;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(3.519863, 101.538116), new LatLng(3.919863, 102.538116));
    GPSTracker PD_gps;
    private int isFlag = 0;
    private StringBuilder builder = new StringBuilder();
    private String finalAddress;
    private List<Address> listaddress;
    private List<Address> address;

    public PDLocation_DialogFragment(int flag) {
        isFlag = flag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Override
    public void onStart(){
        super.onStart();

        Dialog d = getDialog();
        if (d!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pdlocation, container, false);

        PD_mAtv = (EditText)view.findViewById(R.id.PDatv);

        PD_place_listview = (ListView)view.findViewById(R.id.PDdisplay_placeListView);

        Log.d("BgTask", "Establishing Lv");
        B_FPlaceRetrieve_BgroundTask bgTask = new B_FPlaceRetrieve_BgroundTask(getContext(), PD_place_listview);
        bgTask.execute();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        PD_GoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();

        mAutoCompleteAdapter =  new PlacesAutoCompleteAdapter(getContext(), R.layout.search_row,
                PD_GoogleApiClient, BOUNDS_MOUNTAIN_VIEW, null);

        mRecyclerView=(RecyclerView)getView().findViewById(R.id.PDrecyclerView);
        mLinearLayoutManager=new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);

        PD_mAtv.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("") && PD_GoogleApiClient.isConnected()) {
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                    mRecyclerView.setVisibility(View.VISIBLE);
                    PD_place_listview.setVisibility(View.INVISIBLE);
                } else if (!PD_GoogleApiClient.isConnected()) {
                    Toast.makeText(getContext(), "API Not Connected", Toast.LENGTH_SHORT).show();
                }

                if (s.toString().equals("")){
                    mRecyclerView.setVisibility(View.INVISIBLE);
                    PD_place_listview.setVisibility(View.VISIBLE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mAutoCompleteAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.i("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                         */

                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(PD_GoogleApiClient, placeId);
                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if (places.getCount() == 1) {
                                    //Do the things here on Click.....
                                    PD_mAtv.setText(places.get(0).getName());
                                    finalAddress = places.get(0).getAddress().toString();
                                    placeSelected();
                                } else {
                                    Toast.makeText(getActivity(), "Something Wrong", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        Log.i("TAG", "Clicked: " + item.description);
                        Log.i("TAG", "Called getPlaceById to get Place details for " + item.placeId);
                    }
                })
        );

        if (isFlag == 1){
            PD_mAtv.setHint("Enter Pick Up Location");
        }
        else
            PD_mAtv.setHint("Enter Drop Off Location");


        PD_place_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    PD_gps = new GPSTracker(getActivity().getBaseContext());

                    if (PD_gps.canGetLocation()) {
                        double latitude = PD_gps.getLatitude();
                        double longitude = PD_gps.getLongitude();

                        PD_latitude = PD_gps.getLatitude();
                        PD_longitude = PD_gps.getLongitude();

                        updateBox(latitude, longitude);
                        placeSelected();
                    } else {
                        PD_gps.showSettingAlert();
                    }
                } else {

                    Log.d("Here", "Clicked");
                    Place place = (Place) parent.getItemAtPosition(position);
                    updateBox(place.getPlace_latitude(), place.getPlace_longitude());
                    placeSelected();
                }
            }
        });
    };


    public void placeSelected(){
        //converting string to address list
        final Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            address = geocoder.getFromLocationName(PD_mAtv.getText().toString(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent i = new Intent();

        i.putExtra("placeName", PD_mAtv.getText().toString());
        i.putExtra("FinalAddress", finalAddress);
        i.putParcelableArrayListExtra("Address", (ArrayList<Address>) address);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
        dismiss();
    }

    private void updateBox(double curLat, double curLong) {
    Log.d("Updating","Lat " + curLat + " Long " + curLong);
        Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            listaddress = geoCoder.getFromLocation(curLat, curLong, 1);
            Log.d("List Address", "Address" + listaddress);
            int maxLines = listaddress.get(0).getMaxAddressLineIndex();
            builder.setLength(0);//reset builder
            for (int i = 0; i < maxLines; i++) {
                String addressStr = listaddress.get(0).getAddressLine(i);
                builder.append(addressStr);
                builder.append(" ");
            }

            finalAddress = builder.toString(); //This is the complete address.
            PD_mAtv.setText(finalAddress);
            PD_address = finalAddress;

        } catch (IOException e) {

        } catch (NullPointerException e) {

        }
    }

    public void onResume() {
        super.onResume();
        PD_GoogleApiClient.connect();
    }

    public void onPause() {
        super.onPause();
        if (PD_GoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(PD_GoogleApiClient, this);
            PD_GoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        //PD_PlaceArrayAdapter.setGoogleApiClient(PD_GoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {//PD_PlaceArrayAdapter.setGoogleApiClient(null);
    }

    @Override
    public void onLocationChanged(Location location) {
        //
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //
    }
}
