package fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.DirectionsJSONParser;
import com.example.user.mydriver.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ViewHistoryFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback {

    private Reservation reservation;
    private TextView tv_hDate, tv_hSTime, tv_hETime;
    private TextView tv_hOName, tv_hOAddress, tv_hDName, tv_hDAddress;

    private Marker destMarker, pickupMarker;
    private Polyline mapLine;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();

    public ViewHistoryFragment() {
        // Required empty public constructor
    }

    public ViewHistoryFragment(Reservation r){
        reservation = r;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_history, container, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapHistoryView);
        mapFragment.getMapAsync(this);


        tv_hDate = (TextView)view.findViewById(R.id.tv_historyDate);
        tv_hSTime = (TextView)view.findViewById(R.id.tv_historySTime);
        tv_hETime = (TextView)view.findViewById(R.id.tv_historyETime);
        tv_hOName = (TextView)view.findViewById(R.id.tv_historyOriginName);
        tv_hOAddress = (TextView)view.findViewById(R.id.tv_OAddress);
        tv_hDName = (TextView)view.findViewById(R.id.tv_historyDestinationName);
        tv_hDAddress = (TextView)view.findViewById(R.id.tv_hDAddress);

        tv_hDate.setText(reservation.getDate());
        tv_hSTime.setText(reservation.getStartTime());
        tv_hETime.setText(reservation.getEndTime());
        tv_hOName.setText(reservation.getbDetails_OriginName());
        tv_hOAddress.setText(reservation.getOrigin());
        tv_hDName.setText(reservation.getbDetails_DestinationName());
        tv_hDAddress.setText(reservation.getDestination());

        return view;
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);

        buildGoogleApiClient();

        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
            mMap.clear();

            LatLng Origin_latLng = getLatLngFromAddress(tv_hOAddress.getText().toString());
            LatLng Destination_latLng = getLatLngFromAddress(tv_hDAddress.getText().toString());

            Log.d("LatLng", "Origin Lat Lng" + Origin_latLng);

            MarkerOptions originMarker = new MarkerOptions();
            originMarker.position(Origin_latLng);
            originMarker.title("PickUp Location");

            originMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickupmarker));
            pickupMarker = mMap.addMarker(originMarker);

            MarkerOptions destinationMarker = new MarkerOptions();
            destinationMarker.position(Destination_latLng);
            destinationMarker.title("DropOff Location");

            destinationMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
            destMarker = mMap.addMarker(destinationMarker);

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            builder.include(destMarker.getPosition());
            builder.include(pickupMarker.getPosition());

            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 250);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Origin_latLng, 17));
            mMap.animateCamera(cu);

            String url = getDirectionsUrl(Origin_latLng.latitude, Origin_latLng.longitude, Destination_latLng.latitude, Destination_latLng.longitude);
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);

            mMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location) {

    }

    public LatLng getLatLngFromAddress(String strAddress){

        Geocoder coder = new Geocoder(getContext());
        List<Address> address;
        LatLng latLng = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return latLng;
    }

    //Origin latitude, Origin Longtitude, Destination Latitude, Destionation Longtitude
    private String getDirectionsUrl(double mOriginLatitude, double mOriginLongtitude, double mDestinationLatitude, double mDestinationLongtitude){

        LatLng origin = new LatLng(mOriginLatitude, mOriginLongtitude);
        LatLng dest = new LatLng(mDestinationLatitude, mDestinationLongtitude);

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception download url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            if (mapLine != null){
                mapLine.remove();
                mapLine = null;
            }

            if(result.size()<1){
                Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        String distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        String duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }


                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(Color.CYAN);
                lineOptions.geodesic(true);
            }

            //Drawing polyline in the Google Map for the i-th route
            mapLine = mMap.addPolyline(lineOptions);
        }
    }
}
