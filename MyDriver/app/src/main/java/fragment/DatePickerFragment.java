package fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by jihan on 3/2/2016.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    final Calendar c = Calendar.getInstance();
    private int flag;
    private onSetDateFragmentInteraction msSetDateListener;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public DatePickerFragment(int f){
        flag = f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onSetDateFragmentInteraction) {
            msSetDateListener = (onSetDateFragmentInteraction) context;
        }
        else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        c.set(year, monthOfYear, dayOfMonth);
        msSetDateListener.onSetDateFragmentInteraction(c, flag);
    }

    public interface onSetDateFragmentInteraction {
        void onSetDateFragmentInteraction(Calendar cal, int flag);
    }
}
