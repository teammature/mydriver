package fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageButton;

import com.example.user.mydriver.Data.ExistingBooking;
import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.MainActivity;
import com.example.user.mydriver.R;
import com.example.user.mydriver.Adapter.ScheduleAdapter;
import com.parse.ParseObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class DriverFragment extends Fragment implements
        ExistingBooking.onRetrieveBooking {

    private CalendarView calendarView;
    private ImageButton toggleCalendar;
    final private Calendar cal = Calendar.getInstance();
    private List<ParseObject> bookingInfo = new ArrayList<>();
    private SimpleDateFormat minFormat = new SimpleDateFormat("mm", Locale.getDefault());
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.getDefault());
    private ArrayList<Date> timeArr = new ArrayList<>();
    private List<Reservation> bookingList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ScheduleAdapter scheduleAdapter;
    private OnFragmentInteractionListener mListener;
    private ArrayList<Reservation> deleteSlot = new ArrayList<>();

    public DriverFragment() {    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_driver, container, false);

        toggleCalendar = (ImageButton) view.findViewById(R.id.toggleCalendar);
        calendarView = (CalendarView) view.findViewById(R.id.calendarView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        MainActivity.activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        MainActivity.actionBarDrawerToggle.syncState();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
             throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        toggleCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.getVisibility() == View.VISIBLE) {
                    calendarView.setVisibility(View.GONE);
                    toggleCalendar.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                else {
                    calendarView.setVisibility(View.VISIBLE);
                    toggleCalendar.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                cal.set(year, month, dayOfMonth);
                updateSchedule(cal);
            }
        });

        scheduleAdapter = new ScheduleAdapter(bookingList);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(scheduleAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getBaseContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Reservation reservation = bookingList.get(position);
                displayBookingDetails(reservation);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        updateSchedule(Calendar.getInstance());
    }

    //Gets the current time and generate the time blocks for display
    public void updateSchedule(Calendar cal) {
        //Find current time and convert it to closest time
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        Calendar today = Calendar.getInstance();
        Calendar temp;
        Date currDate;
        String time;

        //Clear existing data
        clearData();
        bookingInfo.clear();
        timeArr.clear();

        //Calculate and store the time blocks for display
        //If parameter is before or after today's date, set first block as earliest time
        if(dateFormat.format(cal.getTime()).compareTo(dateFormat.format(today.getTime())) != 0){
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 7, 0, 0);
        }
        else {
            //Reset time slot if time is before 6:00 or exceeds 23:00
            if (today.get(Calendar.HOUR_OF_DAY) < 7 || today.get(Calendar.HOUR_OF_DAY) >= 22) {
                cal.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), 7, 0, 0);
            }
            else { //Start from current time round to **:00 or **:30
                cal.setTime(today.getTime());
                currDate = today.getTime();
                String minute = minFormat.format(today.getTime());
                int minConvert = Integer.parseInt(minute);
                long diff;

                if (minConvert < 30)
                    diff = minConvert;
                else
                    diff = minConvert - 30;

                diff *= 60000;

                Date convertTime = new Date(currDate.getTime() - diff);

                cal.setTime(convertTime);
            }
        }

        //Create initial arraylist while waiting for bookings to be retrieved
        while (true)
        {
            temp = cal;
            timeArr.add(temp.getTime());
            temp.add(Calendar.MINUTE,30);
            time = timeFormat.format(temp.getTime());

            if (time.compareTo("22:00") == 0)   //Break the loop when hit the end point
                break;
        }

        if (!timeArr.isEmpty()) {
            for (int i = 0; i < timeArr.size(); i++) {
                bookingList.add(new Reservation(timeArr.get(i).toString()));
            }
            scheduleAdapter.notifyItemRangeInserted(0, timeArr.size());
            Log.i("Creating schedule", timeArr.size() + " slots created");
        }
        else
            Log.i("Creating schedule", "No slots created");

        new Loading().execute(cal.getTime());
    }

    private class Loading extends AsyncTask<Date, Void, Void> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(getActivity(), "Retrieving", "Please Wait....");
        }

        @Override
        protected Void doInBackground(Date... params) {

            ExistingBooking existingbooking = new ExistingBooking(params[0]);

            existingbooking.setmListener(new ExistingBooking.onRetrieveBooking() {
                @Override
                public void onLoadedBookings(List<ParseObject> results) {
                    if (results.size() > 0)
                        addFoundBooking(results, timeArr);
                }
            });

            return null;
        }

        protected void onPostExecute(Void arg) {
            super.onPostExecute(arg);

            scheduleAdapter.notifyItemRangeChanged(0, timeArr.size());
            recyclerView.scrollToPosition(0); //Reset to first position

            pd.dismiss();
            Log.i("Retrieving bookings", "End of asynctask");
        }
    }

    //Add in current date's existing bookings
    private void addFoundBooking(List<ParseObject> results, ArrayList<Date> arr) {
        final ArrayList<Date> currentTime = new ArrayList<>();
        Date startDate = new Date();
        Date endDate = new Date();
        Date estStartDate = new Date();
        Date estEndDate = new Date();
        ParseObject currBooking;
        String currentT, startTime, endTime, estStart, estEnd;
        boolean start = true, oneWay;
        Reservation r;
        bookingInfo = results;
        Log.i("Passenger bookings", "found: " + bookingInfo.size());

        //Create new schedule arraylist
        ArrayList<Reservation> newList = new ArrayList<>();

        if (arr.isEmpty())
            Log.i("Passenger bookings", "No slots created");
        else {

            for (int i = 0; i < arr.size(); i++) {
                //Format Date to HH:mm
                currentT = timeFormat.format(arr.get(i));
                try {
                    currentTime.add(timeFormat.parse(currentT));

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            }
            //Add row dynamically
            //Set time and location
            for (int i = 0; i < arr.size(); i++) {
                r = new Reservation(arr.get(i).toString());

                //Compare retrieved bookings start time to time block
                if (start) {
                    Iterator<ParseObject> bi_itr = bookingInfo.iterator();

                    while(bi_itr.hasNext()) {
                        currBooking = bi_itr.next();
                        oneWay = currBooking.getBoolean("r_isOneWay");
                        startTime = currBooking.getString("r_StartTime");
                        estStart = currBooking.getString("r_EstimatedPickUpTime");
                        estEnd = currBooking.getString("r_EstimatedEndTripTime");

                        try { //Convert String to Date
                            startDate = timeFormat.parse(startTime);
                            estStartDate = timeFormat.parse(estStart);
                            estEndDate = timeFormat.parse(estEnd);
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }

                        if (oneWay) {
                            //Gray out the slot if it's within estimated pick up time
                            if (currentTime.get(i).compareTo(estStartDate) >= 0 && currentTime.get(i).compareTo(startDate) == -1){
                                r.setbDetails_EstimatedStartTripTime(estStart);
                            } else if (currentTime.get(i).compareTo(startDate) == 1 && currentTime.get(i).compareTo(estEndDate) <= 0) {
                                r.setbDetails_EstimatedEndTripTime(estEnd);
                            } else if (currentTime.get(i).compareTo(startDate) == 0){
                                r.setbDetails_passenger(currBooking.getString("r_Passenger_name"));
                                r.setbDetails_OriginName(currBooking.getString("r_OriginName"));
                                r.setbDetails_DestinationName(currBooking.getString("r_DestinationName"));
                                r.setbDetails_isOneWay(oneWay);
                                r.setbDetails_isCompleted(currBooking.getBoolean("isCompleted"));

                                start = true;
                                break;
                            }
                        } else {
                            //Check if current time is in between estimated pick up time and estimated drop off time
                            if (currentTime.get(i).compareTo(estStartDate) >= 0 && currentTime.get(i).compareTo(estEndDate) <= 0) {

                                //Gray out the slot if it's within estimated pick up time
                                if (currentTime.get(i).compareTo(estStartDate) >= 0 && currentTime.get(i).compareTo(startDate) == -1){
                                    r.setbDetails_EstimatedStartTripTime(estStart);
                                } //Gray out the slot if it's within estimated drop off time
                                else if (currentTime.get(i).compareTo(endDate) == 1 && currentTime.get(i).compareTo(estEndDate) <= 0) {
                                    r.setbDetails_EstimatedEndTripTime(estEnd);
                                } else { //It is within start time to end time
                                    endTime = currBooking.getString("r_EndTime");
                                    try {
                                        endDate = timeFormat.parse(endTime);
                                    } catch (java.text.ParseException e) {
                                        e.printStackTrace();
                                    }

                                    //Rounding up end time for display
                                    String minute = minFormat.format(endDate);
                                    int minConvert = Integer.parseInt(minute);
                                    long diff;

                                    if (minConvert == 0)
                                        diff = 0;
                                    else if (minConvert < 30)
                                        diff = 30 - minConvert;
                                    else
                                        diff = 60 - minConvert;

                                    diff *= 60000;

                                    Date temp = new Date();
                                    temp.setTime(endDate.getTime() + diff);
                                    endTime = timeFormat.format(temp);

                                    r.setbDetails_passenger(currBooking.getString("r_Passenger_name"));
                                    r.setbDetails_OriginName(currBooking.getString("r_OriginName"));
                                    r.setbDetails_DestinationName(currBooking.getString("r_DestinationName"));
                                    r.setbDetails_EndTime(endTime);
                                    r.setbDetails_isOneWay(oneWay);
                                    r.setbDetails_isCompleted(currBooking.getBoolean("isCompleted"));

                                    start = false;
                                    break;
                                }
                            }
                        }
                    }
                    newList.add(r);
                }
                //if the block is part of a booking
                else {
                    if (currentTime.get(i).compareTo(endDate) <= 0)
                        deleteSlot.add(r);

                    if (i < arr.size() - 1 && (endDate.compareTo(currentTime.get(i + 1)) == -1
                            || endDate.compareTo(currentTime.get(i)) == 0))
                        start = true;
                    else
                        start = false;

                    newList.add(r);
                }
            }
            Log.i("Passenger bookings", arr.size() + " slots created");
        }

        Iterator<Reservation> deleteItr = deleteSlot.iterator();
        while (deleteItr.hasNext()) {
            Reservation i = deleteItr.next();
            newList.remove(i);
        }
        clearData();
        bookingList.addAll(newList);
    }

    //Displays existing reservation details, if empty prompt option to perform reservation
    public void displayBookingDetails(Reservation reservation) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        boolean found = false;
        ParseObject booked;
        final List<String> detailList = new ArrayList<>();
        String bookDetails, tripType, note, status = "";
        Date estStartDate = new Date(), estEndDate = new Date();
        Date startDate = new Date(), endDate = new Date(), date = new Date();
        Date curDate = new Date(), curTime = new Date(), resTime = new Date();
        try {
            date = sdf.parse(reservation.getStartTime());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        String reserveTime = timeFormat.format(date);
        String reserveDate = dateFormat.format(date);

        try {
            curDate = dateFormat.parse(dateFormat.format(Calendar.getInstance().getTime()));
            curTime = timeFormat.parse(timeFormat.format(Calendar.getInstance().getTime()));
            date = dateFormat.parse(reserveDate);
            resTime = timeFormat.parse(reserveTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Time slot is earlier than current date
        if (curDate.compareTo(date) == 1) {
            status = "expired";
            found = true;
        }

        Iterator<ParseObject> bi_itr = bookingInfo.iterator();

        while(bi_itr.hasNext()) {
            booked = bi_itr.next();

            try {
                estStartDate = timeFormat.parse(booked.getString("r_EstimatedPickUpTime"));
                estEndDate = timeFormat.parse(booked.getString("r_EstimatedEndTripTime"));
                startDate = timeFormat.parse(booked.getString("r_StartTime"));
                endDate = timeFormat.parse(booked.getString("r_EndTime"));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            //Time slot is earlier than current date
            if (curDate.compareTo(date) == 1) {
                if (resTime.compareTo(estStartDate) >= 0 && resTime.compareTo(estEndDate) <= 0) {
                    //There is booking in time slot
                    if (booked.getBoolean("isCompleted")) {
                        status = "complete";
                        break;
                    }
                } else
                    status = "expired";
                found = true;
            } //Time slot is bigger or equal to current date
            else {
                //Check if it's within estimated start trip and end trip
                if (resTime.compareTo(estStartDate) >= 0 && resTime.compareTo(estEndDate) <= 0) {
                    found = true;

                    //There is booking in time slot
                    if (booked.getBoolean("isCompleted")) {
                        status = "complete";
                        break;
                    }

                    detailList.add(booked.getString("r_Passenger_name"));
                    detailList.add(booked.getString("r_Date"));
                    detailList.add(booked.getString("r_StartTime"));
                    detailList.add(booked.getString("r_Origin"));
                    detailList.add(booked.getString("r_Destination"));

                    if (booked.getBoolean("r_isOneWay")) {
                        tripType = "One way trip";

                        //Check if it's within start time and end time
                        if (resTime.compareTo(startDate) == 0) {
                            if (booked.getString("r_Note").length() > 0)
                                note = getString(R.string.tv_Note) + booked.getString("r_Note");
                            else
                                note = "";

                            bookDetails = getString(R.string.tv_Name) + booked.getString("r_Passenger_name")
                                    + System.getProperty("line.separator") + tripType + System.getProperty("line.separator")
                                    +  note;
                            //Let driver pick up passenger if within estimated start time
                            if (curDate.compareTo(date) == 0 && curTime.compareTo(estStartDate) >= 0 && curTime.compareTo(estEndDate) < 0) {
                                new AlertDialog.Builder(getActivity()).setTitle("Booking details").setMessage(bookDetails)
                                        .setPositiveButton("Pick up now", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                mListener.onFragmentChanged(2, detailList);
                                            }
                                        })
                                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                            } else {
                                new AlertDialog.Builder(getActivity()).setTitle("Booking details").setMessage(bookDetails)
                                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                            }
                            break;
                        }
                    } else {
                        tripType = "Round trip";

                        //Check if it's within start time and end time
                        if (resTime.compareTo(startDate) >= 0 && resTime.compareTo(endDate) <= 0) {
                            if (booked.getString("r_Note").length() > 0)
                                note = getString(R.string.tv_Note) + booked.getString("r_Note");
                            else
                                note = "";

                            bookDetails = getString(R.string.tv_Name) + booked.getString("r_Passenger_name")
                                    + System.getProperty("line.separator") + tripType + System.getProperty("line.separator")
                                    +  note;

                            //Let driver pick up passenger if within estimated start time
                            if (curTime.compareTo(estStartDate) >= 0 && curTime.compareTo(estEndDate) <= 0) {
                                new AlertDialog.Builder(getActivity()).setTitle("Booking details").setMessage(bookDetails)
                                        .setPositiveButton("Pick up now", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                mListener.onFragmentChanged(2, detailList);
                                            }
                                        })
                                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                            } else {
                                new AlertDialog.Builder(getActivity()).setTitle("Booking details").setMessage(bookDetails)
                                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                            }
                            break;
                        }
                    }

                    new AlertDialog.Builder(getActivity()).setTitle("Time slot unavailable").setMessage("Time slot has been reserved or is unavailable.")
                            .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).create().show();
                }
            }
        }

        if (status.equals("expired")) {
            new AlertDialog.Builder(getActivity()).setTitle("Time slot unavailable").setMessage("Time slot is earlier than current time.")
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).create().show();
        }

        if (!found) { //Prompt option to book available slot
            final List<String> list = new ArrayList<>();
            list.add(reserveDate);
            list.add(reserveTime);

            AlertDialog.Builder bookingDialog = new AlertDialog.Builder(getActivity());
            bookingDialog.setMessage("No existing booking." + System.getProperty("line.separator") + "Do you want to book this time slot?");
            bookingDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mListener.onFragmentChanged(1, list);
                }
            });
            bookingDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = bookingDialog.create();
            alert.show();
        }
    }

    @Override
    public void onLoadedBookings(List<ParseObject> results) {
        bookingInfo = results;
        Log.i("onLoadedBookingResults", "Retrieved size: " + bookingInfo.size());
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public interface OnFragmentInteractionListener{
        void onFragmentChanged(int i, List<String> list);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private DriverFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final DriverFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public void clearData() {
        int size = bookingList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                bookingList.remove(0);
            }

            scheduleAdapter.notifyItemRangeRemoved(0, size);
        }
    }
}
