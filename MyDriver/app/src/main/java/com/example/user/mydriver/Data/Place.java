package com.example.user.mydriver.Data;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.mydriver.R;
import com.example.user.mydriver.dbFavouritePlaceOperations;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Owner on 4/27/2016.
 */
public class Place {

    public static abstract class newPlace {
        // Labels Table Columns names
        public static final String KEY_placename = "name";
        public static final String KEY_image = "image_data";
        public static final String KEY_latitude = "lat";
        public static final String KEY_longitude = "lng";
        public static final String KEY_address = "address";
        public static final String TABLENAME = "FavouritePlace";
    }

    // property help us to keep data
    public String place_Name;
    public String place_Image;
    public double place_latitude;
    public double place_longitude;
    public String place_Address;
    public int place_id;

    public Place(String name, String image, double lat, double lng, String address){
        place_Name = name;
        place_Image = image;
        place_latitude = lat;
        place_longitude = lng;
        place_Address = address;
    };

    public Place(String name, String image, double lat, double lng, String address, int id){
        place_Name = name;
        place_Image = image;
        place_latitude = lat;
        place_longitude = lng;
        place_Address = address;
        place_id = id;
    };

    public int getPlace_ID(){return place_id;};

    public String getPlace_Name(){
        return place_Name;
    }

    public String getPlace_Address() {return place_Address;}

    public String getPlace_Image(){
        return place_Image;
    }

    public double getPlace_latitude(){
        return place_latitude;
    }

    public double getPlace_longitude(){
        return place_longitude;
    }
}
