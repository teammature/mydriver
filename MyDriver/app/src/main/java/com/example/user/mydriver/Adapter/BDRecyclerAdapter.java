package com.example.user.mydriver.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner on 5/15/2016.
 */

//booking details recycler adapter
public class BDRecyclerAdapter extends RecyclerView.Adapter<BDRecyclerAdapter.ReservationHolder> {

    List<Reservation> reservations = new ArrayList<Reservation>();

    public BDRecyclerAdapter(){};

    public BDRecyclerAdapter(List<Reservation> reservations){
        this.reservations = reservations;
    }

    @Override
    public ReservationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookingdetails_row, parent, false);
        ReservationHolder rh = new ReservationHolder(v);
        return rh;
    }

    @Override
    public void onBindViewHolder(ReservationHolder holder, int position) {

        Log.d("onBindViewHolder", "Setting Date = " + reservations.get(position).getDate());
        holder.tx_date.setText(reservations.get(position).getDate());
        holder.tx_starttime.setText(reservations.get(position).getStartTime());
        holder.tx_endtime.setText(reservations.get(position).getEndTime());
        holder.tx_origin.setText(reservations.get(position).getbDetails_OriginName());
        holder.tx_destination.setText(reservations.get(position).getbDetails_DestinationName());
    }


    @Override
    public int getItemCount() {
        if(reservations != null)
            return reservations.size();
        else
            return 0;
    }

    public Reservation getItem(int position) {
        return reservations.get(position);
    }

    public static class ReservationHolder extends RecyclerView.ViewHolder {
        TextView tx_date, tx_starttime, tx_endtime, tx_origin, tx_destination;

        public ReservationHolder(View itemView) {
            super(itemView);
            tx_date = (TextView)itemView.findViewById(R.id.bdisplay_date);
            tx_starttime = (TextView)itemView.findViewById(R.id.bdisplay_stime);
            tx_endtime = (TextView)itemView.findViewById(R.id.bdisplay_etime);
            tx_origin = (TextView)itemView.findViewById(R.id.bdisplay_origin);
            tx_destination = (TextView)itemView.findViewById(R.id.bdisplay_destination);
        }
    }
}
