package com.example.user.mydriver.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.mydriver.Data.Reservation;
import com.example.user.mydriver.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyViewHolder> {
    private List<Reservation> bookingList;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView startTime, endTime, origin, destination;
        public ImageView timeSeparator, locationSeparator;
        public LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            linearLayout = (LinearLayout) view.findViewById(R.id.br_LinearLayoutParent);
            timeSeparator = (ImageView) view.findViewById(R.id.br_ivTimeLine);
            locationSeparator = (ImageView) view.findViewById(R.id.br_ivLocLine);
            startTime = (TextView) view.findViewById(R.id.booking_time);
            endTime = (TextView) view.findViewById(R.id.booking_endTime);
            origin = (TextView) view.findViewById(R.id.br_from);
            destination = (TextView) view.findViewById(R.id.br_destination);
        }
    }

    public ScheduleAdapter(List<Reservation> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_row, parent, false);
        return new MyViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Reservation reservation = bookingList.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.getDefault());

        Date d1 = new Date();
        try {
            d1 = sdf.parse(reservation.getStartTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String b_time = timeFormat.format(d1);

        holder.startTime.setText(b_time);
        holder.endTime.setText(reservation.getEndTime());
        holder.origin.setText(reservation.getbDetails_OriginName());
        holder.destination.setText(reservation.getbDetails_DestinationName());

        if (holder.origin.getText().toString().isEmpty()) {
            holder.destination.setVisibility(View.GONE);
            holder.endTime.setVisibility(View.GONE);
            holder.locationSeparator.setVisibility(View.GONE);

            if (!reservation.getbDetails_EstimatedStartTripTime().isEmpty()){
                holder.linearLayout.setBackgroundResource(R.color.grey);
            } else if (!reservation.getbDetails_EstimatedEndTripTime().isEmpty()) {
                holder.linearLayout.setBackgroundResource(R.color.grey);
            } else
                holder.linearLayout.setBackgroundResource(R.color.white);
        } else {
            holder.locationSeparator.setVisibility(View.VISIBLE);
            holder.destination.setVisibility(View.VISIBLE);

            if (reservation.getCompleted()) {
                holder.linearLayout.setBackgroundResource(R.color.grey);
            }
            else {
                holder.linearLayout.setBackgroundResource(R.color.white);
            }
        }

        if (!holder.endTime.getText().toString().isEmpty()) {
            holder.timeSeparator.setVisibility(View.VISIBLE);
            holder.endTime.setVisibility(View.VISIBLE);
        }
        else
            holder.timeSeparator.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }
}