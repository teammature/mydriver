package com.example.user.mydriver.BackgroundTask;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;

import com.example.user.mydriver.Adapter.B_FPlaceArrayAdapter;
import com.example.user.mydriver.Data.Place;
import com.example.user.mydriver.R;
import com.example.user.mydriver.dbFavouritePlaceOperations;

import java.io.ByteArrayOutputStream;
import java.util.List;

import fragment.PDLocation_DialogFragment;

/**
 * Created by Owner on 5/22/2016.
 */
public class B_FPlaceRetrieve_BgroundTask extends AsyncTask<String, Place, String> {
    private Context ctx;
    private static B_FPlaceArrayAdapter arrayAdapter;
    private Activity activity;
    private ListView recyclerlistview;

    public B_FPlaceRetrieve_BgroundTask(Context ctx, ListView view){
        this.ctx = ctx;
        activity = (Activity)ctx;
        recyclerlistview = view;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        dbFavouritePlaceOperations dbp = new dbFavouritePlaceOperations(ctx);

        arrayAdapter = new B_FPlaceArrayAdapter(ctx, R.layout.display_place_row);

        SQLiteDatabase db = dbp.getReadableDatabase();
        Cursor cursor = dbp.getPlaceDetails(db);

        String pname, paddress, pimage;
        double plat, plng;

        Log.d("Retrieve", "Processing");
        Bitmap bitmapimage = null;

        if (arrayAdapter.isEmpty()){

            bitmapimage = BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.ic_my_location_black_18dp);

            // convert bitmap to byte
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapimage.compress(Bitmap.CompressFormat.PNG, 100, stream);

            pname = "Current Location";
            pimage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
            plat = 0;
            plng = 0;
            paddress = null;
            Place newPlace = new Place(pname, pimage, plat, plng, paddress);
            publishProgress(newPlace);
        }

        while (cursor.moveToNext()){
            pname = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_placename));
            pimage = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_image));
            plat = cursor.getDouble(cursor.getColumnIndex(Place.newPlace.KEY_latitude));
            plng = cursor.getDouble(cursor.getColumnIndex(Place.newPlace.KEY_longitude));
            paddress = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_address));
            Place newPlace = new Place(pname, pimage, plat, plng, paddress);
            publishProgress(newPlace);
            Log.d("Name", newPlace.place_Name);
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Place... values) {
        arrayAdapter.add(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        recyclerlistview.setAdapter(arrayAdapter);
    }
}
