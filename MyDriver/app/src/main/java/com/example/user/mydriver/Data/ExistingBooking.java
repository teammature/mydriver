package com.example.user.mydriver.Data;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ExistingBooking {
    private onRetrieveBooking mListener;

    public ExistingBooking(Date mDate) {
        this.mListener = null;
        retrieveExistingBooking(mDate);
    }

    public void setmListener(onRetrieveBooking mListener) {
        this.mListener = mListener;
    }

    public interface onRetrieveBooking {
        void onLoadedBookings(List<ParseObject> results);
    }

    //Retrieves all bookingInfo on selected date
    //from Parse as array of ParseObjects
    public void retrieveExistingBooking(Date date){
        Calendar calendar = Calendar.getInstance();
        Date date1, date2;

        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 6);
        date1 = calendar.getTime();

        calendar.set(Calendar.HOUR, 23);
        date2 = calendar.getTime();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Reservation");

        query.whereGreaterThanOrEqualTo("Date", date1);
        query.whereLessThanOrEqualTo("Date", date2);

        final List<ParseObject> results = new ArrayList<>();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        results.add(list.get(i));
                    }
                    mListener.onLoadedBookings(results);
                }
            }
        });
    }
}
