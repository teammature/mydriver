package com.example.user.mydriver.Data;

/**
 * Created by Owner on 5/15/2016.
 */
public class Reservation {
    private String bDetails_objectId;
    private String bDetails_passenger = "";
    private String bDetails_Date;
    private String bDetails_StartTime;
    private String bDetails_EstimatedStartTripTime;
    private String bDetails_EstimatedEndTripTime;
    private String bDetails_EndTime;
    private String bDetails_OriginName;
    private String bDetails_DestinationName;
    private String bDetails_Origin;
    private String bDetails_Destination;
    private Boolean bDetails_isOneWay;
    private String bDetails_Note;
    private Boolean bDetails_isCompleted = false;

    public Reservation(){
        bDetails_Date = "";
        bDetails_StartTime = "";
        bDetails_EstimatedStartTripTime = "";
        bDetails_EstimatedEndTripTime = "";
        bDetails_EndTime = "";
        bDetails_OriginName = "";
        bDetails_DestinationName = "";
        bDetails_Origin = "";
        bDetails_Destination = "";
        bDetails_isOneWay = false;
        bDetails_Note = "";
        bDetails_isCompleted = false;
    }

    public Reservation(String startTime) {
        bDetails_StartTime = startTime;
        bDetails_Date = "";
        bDetails_EstimatedStartTripTime = "";
        bDetails_EstimatedEndTripTime = "";
        bDetails_EndTime = "";
        bDetails_OriginName = "";
        bDetails_DestinationName = "";
        bDetails_Origin = "";
        bDetails_Destination = "";
        bDetails_isOneWay = false;
        bDetails_Note = "";
        bDetails_isCompleted = false;
    }

    //constructor for passenger/driver
    public Reservation(String date, String startTime) {
        bDetails_Date = date;
        bDetails_StartTime = startTime;
    }

    //constructor for historyfragment
    public Reservation(String date, String r_startTime,
                       String r_originName, String r_originAddress, String r_destinationName, String r_desitnationAddress,
                       boolean r_isOneWay, String r_endTime) {
        bDetails_Date = date;
        bDetails_StartTime = r_startTime;
        bDetails_OriginName = r_originName;
        bDetails_Origin = r_originAddress;
        bDetails_DestinationName = r_destinationName;
        bDetails_Destination = r_desitnationAddress;
        bDetails_isOneWay = r_isOneWay;
        bDetails_EndTime = r_endTime;
    }

    //constructor to check available timeslot
    public Reservation(Boolean isOneway, String Origin, String Destination, String estimatedPickUpTime, String estimatedReturnTime){
        bDetails_EstimatedStartTripTime = estimatedPickUpTime;
        bDetails_EstimatedEndTripTime = estimatedReturnTime;
        bDetails_Origin = Origin;
        bDetails_Destination = Destination;
        bDetails_isOneWay = isOneway;
    }

    //constructor for managebookingfragment
    public Reservation(String objectID, String date,
                       String r_estimatepickup, String r_startTime, String r_endTime, String r_estimateendtrip,
                       String r_originName, String r_originAddress, String r_destinationName, String r_desitnationAddress,
                       boolean r_isOneWay, String r_note) {

        bDetails_objectId = objectID;
        bDetails_Date = date;
        bDetails_EstimatedStartTripTime = r_estimatepickup;
        bDetails_StartTime = r_startTime;
        bDetails_EndTime = r_endTime;
        bDetails_EstimatedEndTripTime = r_estimateendtrip;
        bDetails_OriginName = r_originName;
        bDetails_Origin = r_originAddress;
        bDetails_DestinationName = r_destinationName;
        bDetails_Destination = r_desitnationAddress;
        bDetails_isOneWay = r_isOneWay;
        bDetails_Note = r_note;

    }

    public String getObject_Id(){
        return bDetails_objectId;
    }

    public String getPassenger() { return bDetails_passenger; }

    public String getDate(){
        return bDetails_Date;
    }

    public String getStartTime(){
        return bDetails_StartTime;
    }

    public String getbDetails_EstimatedEndTripTime(){
        return bDetails_EstimatedEndTripTime;
    }

    public String getbDetails_EstimatedStartTripTime(){return bDetails_EstimatedStartTripTime;}

    public String getbDetails_OriginName(){return bDetails_OriginName;}

    public String getOrigin(){
        return bDetails_Origin;
    }

    public String getbDetails_DestinationName(){return bDetails_DestinationName;}

    public String getDestination(){
        return bDetails_Destination;
    }

    public String getEndTime(){
        return bDetails_EndTime;
    }

    public Boolean getOneWay(){
        return bDetails_isOneWay;
    }

    public Boolean getCompleted() { return bDetails_isCompleted; }

    public String getbDetails_Note(){
        return bDetails_Note;
    }

    public void setbDetails_objectId(String objectId){
        bDetails_objectId = objectId;
    };

    public void setbDetails_passenger(String passenger) { bDetails_passenger = passenger; }

    public void setbDetails_Date(String date){
        bDetails_Date = date;
    };

    public void setbDetails_StartTime(String startTime){
        bDetails_StartTime = startTime;
    };

    public void setbDetails_EstimatedStartTripTime(String estimateSTime){
        bDetails_EstimatedStartTripTime = estimateSTime;
    };

    public void setbDetails_EstimatedEndTripTime(String estimateETime){
        bDetails_EstimatedEndTripTime = estimateETime;
    };

    public void setbDetails_EndTime(String endTime){
        bDetails_EndTime= endTime;
    };

    public void setbDetails_OriginName(String name){bDetails_OriginName = name;}

    public void setbDetails_Origin(String origin){
        bDetails_Origin = origin;
    };

    public void setbDetails_Destination(String destination){
        bDetails_Destination = destination;
    };

    public void setbDetails_DestinationName(String name){bDetails_DestinationName = name;}

    public void setbDetails_Note(String note){
        bDetails_Note = note;
    };

    public void setbDetails_isOneWay(Boolean isOneWay){
        bDetails_isOneWay = isOneWay;
    };

    public void setbDetails_isCompleted(Boolean isCompleted){
        bDetails_isCompleted = isCompleted;
    };
}
