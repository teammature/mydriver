package com.example.user.mydriver.BackgroundTask;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.*;

import fragment.PDLocation_DialogFragment;

import com.example.user.mydriver.R;
import com.example.user.mydriver.Data.Place;
import com.example.user.mydriver.Adapter.FavouritePlace_ArrayAdapter;
import com.example.user.mydriver.dbFavouritePlaceOperations;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * Created by Owner on 5/1/2016.
 */
public class PlaceRetrieve_BgroundTask extends AsyncTask<String, Place, String> {
    private Context ctx;
    private static FavouritePlace_ArrayAdapter favouritePlaceArrayAdapter;
    private PDLocation_DialogFragment fragment;
    private Activity activity;
    private static ListView lv;
    private ListView listview;
    private static ListView bookingLV;
    private static ListView favouriteLV;

    public PlaceRetrieve_BgroundTask(Context ctx, ListView view){
        this.ctx = ctx;
        activity = (Activity)ctx;
        listview = view;
    }

    public PlaceRetrieve_BgroundTask(Context ctx){
        this.ctx = ctx;
        activity = (Activity)ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        String method = params[0];

        dbFavouritePlaceOperations dbp = new dbFavouritePlaceOperations(ctx);

        if (method.equals("add_place")){
            String pname = params[1];
            String pimage = params[2];
            String plat = params[3];
            String plng = params[4];
            String padd = params[5];
            int pid = Integer.parseInt(params[6]);


            Log.d("BMP", "params[2]" + params[2].getBytes());
            Log.d("BMP", "params[2]" + pimage);
            SQLiteDatabase sqLiteDatabse = dbp.getWritableDatabase();
            String result = dbp.addPlace(pname, pimage, plat, plng, padd, pid, sqLiteDatabse);
            return result;
        }
        else if (method.equals("show_place")){
            favouritePlaceArrayAdapter = new FavouritePlace_ArrayAdapter(ctx, R.layout.display_place_row);

            favouriteLV = (ListView) activity.findViewById(R.id.lv_favouritePlace);
            SQLiteDatabase db = dbp.getReadableDatabase();

            List<Place> lv_favourite;

            String pname, paddress, pimage;
            double plat, plng;
            Bitmap bitmapimage = null;

            Log.d("New List View", "ListView Processing");

            if (!dbp.placeExist("Home", db)){

                bitmapimage = BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.ic_home_black_18dp);

                // convert bitmap to byte
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapimage.compress(Bitmap.CompressFormat.PNG, 100, stream);

                pname = "Add Home";
                pimage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                plat = 0;
                plng = 0;
                paddress = null;
                Place newPlace = new Place(pname, pimage, plat, plng, paddress);
                publishProgress(newPlace);
            }

            else{
                Place home = dbp.getPlace("Home", db);
                publishProgress(home);
            }


            if (!dbp.placeExist("Work", db)){

                bitmapimage = BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.ic_work_black_18dp);

                // convert bitmap to byte
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapimage.compress(Bitmap.CompressFormat.PNG, 100, stream);

                pname = "Add Work";
                pimage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                plat = 0;
                plng = 0;
                paddress = null;
                Place newPlace = new Place(pname, pimage, plat, plng, paddress);
                publishProgress(newPlace);

                Log.d("New List View", "ListView Work doesn't exist");
            }

            else{
                Place work = dbp.getPlace("Work", db);
                publishProgress(work);

                Log.d("New List View", "ListView Work exist");
            }

            if (!dbp.placeExist("Favourite", db)){

                bitmapimage = BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.ic_star_border_black_18dp);

                // convert bitmap to byte
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapimage.compress(Bitmap.CompressFormat.PNG, 100, stream);

                pname = "Add Favourite";
                pimage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                plat = 0;
                plng = 0;
                paddress = null;
                Place newPlace = new Place(pname, pimage, plat, plng, paddress);
                publishProgress(newPlace);

                Log.d("New List View", "ListView Processing Favourite");
            }

            else{
                lv_favourite = dbp.getFavouritePlace("Favourite", db);
                for (int i = 0; i <lv_favourite.size(); i++){
                    publishProgress(lv_favourite.get(i));
                }
            }
            return "show_place";
        }

        else if (method.equals("get_Info")){
            favouritePlaceArrayAdapter = new FavouritePlace_ArrayAdapter(ctx, R.layout.display_place_row);

            lv = (ListView) activity.findViewById(R.id.display_placeListView);
            SQLiteDatabase db = dbp.getReadableDatabase();
            Cursor cursor = dbp.getPlaceDetails(db);

            String pname, paddress, pimage;
            int pid;
            double plat, plng;

            Log.d("Retrieve", "Processing");

            while (cursor.moveToNext()){
                pid = cursor.getInt(cursor.getColumnIndex("rowid"));
                pname = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_placename));
                pimage = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_image));
                plat = cursor.getDouble(cursor.getColumnIndex(Place.newPlace.KEY_latitude));
                plng = cursor.getDouble(cursor.getColumnIndex(Place.newPlace.KEY_longitude));
                paddress = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_address));
                Log.d("primary key", "key = " + pid);
                Place newPlace = new Place(pname, pimage, plat, plng, paddress, pid);
                publishProgress(newPlace);
                Log.d("Name", newPlace.place_Name);
            }
            return "get_Info";
        }

        else if (method.equals("get_Info_Booking")){
            favouritePlaceArrayAdapter = new FavouritePlace_ArrayAdapter(ctx, R.layout.display_place_row);

            SQLiteDatabase db = dbp.getReadableDatabase();
            Cursor cursor = dbp.getPlaceDetails(db);

            String pname, paddress,pimage;
            double plat, plng;

            Log.d("Retrieve", "Processing");

            while (cursor.moveToNext()){
                pname = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_placename));
                pimage = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_image));
                plat = cursor.getDouble(cursor.getColumnIndex(Place.newPlace.KEY_latitude));
                plng = cursor.getDouble(cursor.getColumnIndex(Place.newPlace.KEY_longitude));
                paddress = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_address));
                Place newPlace = new Place(pname, pimage, plat, plng, paddress);
                publishProgress(newPlace);
                Log.d("Name", newPlace.place_Name);
            }
            return "get_Info_Booking";
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Place... values) {
        favouritePlaceArrayAdapter.add(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equals("get_Info")){
            Log.d("Set Adapter", "Progressing");
            lv.setAdapter(favouritePlaceArrayAdapter);
        }

        else if (result.equals("get_Info_Booking")){

            listview.setAdapter(favouritePlaceArrayAdapter);
        }

        else if (result.equals("show_place")){
            favouriteLV.setAdapter(favouritePlaceArrayAdapter);
        }
        else{
            // TODO: 5/2/2016  -- notify adapater array
            /*
            favouritePlaceArrayAdapter.clear();
            favouritePlaceArrayAdapter.notifyDataSetChanged();
            lv.setAdapter(favouritePlaceArrayAdapter);*/
            Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
        }
    }
}
