package com.example.user.mydriver.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.mydriver.Data.Place;
import com.example.user.mydriver.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner on 5/22/2016.
 * //Booking Favourite Place Array Adapter
 */
public class B_FPlaceArrayAdapter extends ArrayAdapter{
    List list = new ArrayList();
    Context context;

    public B_FPlaceArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Place object){
        list.add(object);
        super.add(object);
    }

    @Override
    public int getCount(){
        return list.size();
    };

    @Override
    public Object getItem(int position){
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        PlaceHolder placeHolder;

        if (row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.display_place_row, parent, false);
            placeHolder = new PlaceHolder();
            placeHolder.tx_placeImage = (ImageView)row.findViewById(R.id.place_image);
            placeHolder.tx_placeName = (TextView) row.findViewById(R.id.v_placeName);
            placeHolder.tx_placeAddress = (TextView) row.findViewById(R.id.v_placeAddress);
            row.setTag(placeHolder);
        }
        else{
            placeHolder = (PlaceHolder) row.getTag();
        }


        Place place = (Place) getItem(position);
        byte[] decodedString = Base64.decode(place.getPlace_Image(), 0);
        Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        Log.d("BMP", "BMP " + bmp);
        placeHolder.tx_placeImage.setImageBitmap(bmp);
        placeHolder.tx_placeName.setText(place.getPlace_Name());
        placeHolder.tx_placeAddress.setText(place.getPlace_Address());

        return row;
    }

    static class PlaceHolder{
        TextView tx_placeName, tx_placeAddress;
        ImageView tx_placeImage;
    }
}
