package com.example.user.mydriver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.user.mydriver.Data.Place;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Owner on 4/27/2016.
 */
public class dbFavouritePlaceOperations extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "FavouritePlace.db";

    //Create a FTS3 Virtual Table for fast searches
    private static final String CREATE_TABLE_PLACE =
            "CREATE VIRTUAL TABLE " + Place.newPlace.TABLENAME + " USING fts3("
                    + Place.newPlace.KEY_placename + " TEXT,"
                    + Place.newPlace.KEY_image + " BLOB,"
                    + Place.newPlace.KEY_latitude + " TEXT,"
                    + Place.newPlace.KEY_longitude + " TEXT,"
                    + Place.newPlace.KEY_address + " TEXT);";

    public dbFavouritePlaceOperations(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("Database Place", "Database Created/Opened");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PLACE);

        Log.d("Database Place", "Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + Place.newPlace.TABLENAME);

        onCreate(db);
    }

    public String addPlace(String name, String image, String lat, String lng, String address, int pid, SQLiteDatabase db) {
        boolean isAdding = false;

        if (pid == 0) {
            isAdding = true;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(Place.newPlace.KEY_placename, name);
        contentValues.put(Place.newPlace.KEY_image, image);
        contentValues.put(Place.newPlace.KEY_latitude, lat);
        contentValues.put(Place.newPlace.KEY_longitude, lng);
        contentValues.put(Place.newPlace.KEY_address, address);

        Log.d("Database Operation", name + " Inserted");

        String Query = "Select * from " + Place.newPlace.TABLENAME
                + " where rowid =" + pid;
        Cursor cursor = db.rawQuery(Query, null);

        //false
        if (isAdding && cursor.getCount() <= 0) {
            db.insert(Place.newPlace.TABLENAME, null, contentValues);
            cursor.close();
            db.close();
            return name + " Added";
        }
        else {
            db.update(Place.newPlace.TABLENAME, contentValues, "rowid = " + pid, null);
            cursor.close();
            db.close();
            return name + " Updated";
        }
    }

    //check place exist
    public boolean placeExist(String name, SQLiteDatabase db){
        String Query = "Select * from " + Place.newPlace.TABLENAME
                + " where " + Place.newPlace.KEY_placename + "=" + "'" + name + "'";
        Cursor cursor = db.rawQuery(Query, null);

        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    //get Place
    public Place getPlace(String name, SQLiteDatabase db){

        Place newplace = null;
        int pid;

        String Query = "Select * from " + Place.newPlace.TABLENAME
                + " where " + Place.newPlace.KEY_placename + "=" + "'" + name + "'";
        Cursor cursor = db.rawQuery(Query, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    pid = cursor.getColumnIndex("rowid");
                    String pname = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_placename));
                    String pimage = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_image));
                    String plat = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_latitude));
                    String plng = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_longitude));
                    String padd = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_address));

                    Log.d("db place", "db name " + pname);
                    newplace = new Place(pname, pimage, Double.valueOf(plat), Double.valueOf(plng), padd, pid);
                }while(cursor.moveToNext());
            }
        }
        else{
            return null;
        }
        return newplace;
    }

    //get Place
    public List<Place> getFavouritePlace(String name, SQLiteDatabase db){

        ArrayList<Place> listplace = new ArrayList<Place>();

        String Query = "Select * from " + Place.newPlace.TABLENAME
                + " where " + Place.newPlace.KEY_placename + "=" + "'" + name + "'";
        Cursor cursor = db.rawQuery(Query, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int pid = cursor.getColumnIndex("rowid");
                    String pname = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_placename));
                    String pimage = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_image));
                    String plat = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_latitude));
                    String plng = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_longitude));
                    String padd = cursor.getString(cursor.getColumnIndex(Place.newPlace.KEY_address));

                    Place newplace = new Place(pname, pimage, Double.valueOf(plat), Double.valueOf(plng), padd, pid);
                    listplace.add(newplace);
                }while(cursor.moveToNext());
            }
        }
        else{
            return null;
        }

        return listplace;
    }

    //get all the place details
    public Cursor getPlaceDetails(SQLiteDatabase db){
        String[] projections = {"rowid", Place.newPlace.KEY_placename, Place.newPlace.KEY_image,
                Place.newPlace.KEY_latitude, Place.newPlace.KEY_longitude,
                Place.newPlace.KEY_address};

        Cursor cursor = db.query(Place.newPlace.TABLENAME, projections,null, null, null, null, null);
        return cursor;
    };
}
