package com.example.user.mydriver;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mydriver.BackgroundTask.PlaceRetrieve_BgroundTask;
import com.example.user.mydriver.Data.Reservation;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import fragment.BookingDetails_Fragment;
import fragment.DatePickerFragment;
import fragment.DriverFragment;
import fragment.HistoryFragment;
import fragment.LocationFragment;
import fragment.ManageBookingFragment;
import fragment.Profile.ProfileEditableFragment;
import fragment.PickUpFragment;

import fragment.Profile.ProfileFragment;
import fragment.PassengerFragment;
import fragment.TimePickerFragment;
import fragment.addPlaceFragment;
import fragment.temp_fragment;
import fragment.todayScheduleFragment;
import fragment.upcomingScheduleFragment;

public class MainActivity extends AppCompatActivity
        implements LocationFragment.OnFragmentInteractionListener,
        DatePickerFragment.onSetDateFragmentInteraction,
        TimePickerFragment.OnFragmentInteractionListener,
        PassengerFragment.OnFragmentInteractionListener,
        DriverFragment.OnFragmentInteractionListener,
        PickUpFragment.OnFragmentInteractionListener,
        addPlaceFragment.OnFragmentInteractionListener,
        ProfileEditableFragment.OnFragmentInteractionListener,
        todayScheduleFragment.OnFragmentInteractionListener,
        upcomingScheduleFragment.OnFragmentInteractionListener,
        temp_fragment.OnFragmentInteractionListener{

    public static MainActivity activity;
    private boolean isPassenger;
    private TextView tv_username, tv_usercharacter;
    private ParseFile imageFile;
    private DrawerLayout drawerLayout;
    public static Toolbar toolbar;
    public static ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;
    private FragmentTransaction fragmentTransaction;
    private boolean doubleBackToExitPressedOnce = false;
    private String username;
    SQLiteDatabase sqLiteDabase;
    ParseUser currentUser = ParseUser.getCurrentUser();


    private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_main);

        activity = MainActivity.this;

        if (ParseUser.getCurrentUser() == null){
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
        }

        else {
            ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
            username = ParseUser.getCurrentUser().getUsername();
            parseInstallation.put("username", username);
            parseInstallation.saveInBackground();

            Log.d("Checking Parse", "installed");
        }

        //---------------------------------Set Up ToolBar-------------------------------------------
        try {
            isPassenger = ParseUser.getCurrentUser().getBoolean("isPassenger");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (isPassenger)
            fragmentTransaction.add(R.id.fragment_Container, new PassengerFragment(), "Home");
        else
            fragmentTransaction.add(R.id.fragment_Container, new DriverFragment(), "Home");

        fragmentTransaction.commit();


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close);

        MainActivity.activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        //--------------------------------Set Up Navigation Bar-------------------------------------
        //------------------------------------------------------------------------------------------
        getSupportActionBar().setTitle("Home");

        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        final View header = navigationView.getHeaderView(0);
        tv_username = (TextView)header.findViewById(R.id.header_username);
        tv_usercharacter = (TextView)header.findViewById(R.id.header_usercharacter);


        //Retrieving the Profile Picture from Parse
        imageFile = (ParseFile) currentUser.get("profilePicture");

        if (imageFile != null) {

            imageFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    ImageView profilePicture = (ImageView) header.findViewById(R.id.profile_pic);
                    profilePicture.setImageBitmap(bitmap);
                }
            });
        }


        Log.d("Username", "name " + username);
        tv_username.setText(username);

        if (isPassenger)
            tv_usercharacter.setText("Passenger");
        else
            tv_usercharacter.setText("Driver");


        //----------------------------------Navigation Bar Items------------------------------------
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_id: {
                        if (isPassenger)
                            changeFragment(new PassengerFragment(), "Home", false);
                        else
                            changeFragment(new DriverFragment(), "Home", false);
                        break;
                    }

                    case R.id.booking_id: {
                        //changeFragment(new BookingFragment("NewBooking", username, "null", "null"), "New Booking", true);
                        Reservation reservation = new Reservation();
                        changeFragment(new BookingDetails_Fragment("Add", reservation), "New Booking", true);
                        //changeFragment(new temp_fragment(), "New Booking", true);
                        break;
                    }

                    case R.id.managebooking_id: {
                        changeFragment(new ManageBookingFragment(), "Manage Booking", true);
                        break;
                    }

                    case R.id.profile_id: {
                        changeFragment(new ProfileFragment(), "Profile", true);
                        break;
                    }

                    case R.id.history_id: {
                        changeFragment(new HistoryFragment(), "History", true);
                        break;
                    }

                    case R.id.logout_id: {
                        drawerLayout.closeDrawers();

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Exit Application and Log Out");
                        builder.setMessage("Do you want to log out ?");


                        // Set up the buttons
                        builder.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ParseUser.getCurrentUser().logOut();
                                finish();
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();

                        break;
                    }
                }
                return false;
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void changeFragment(Fragment fragment, String title, boolean isAddToBackStack){
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_Container, fragment, title);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(null);
            actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            drawerLayout.closeDrawers();
        } else {
            actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            actionBarDrawerToggle.syncState();
            drawerLayout.closeDrawers();
        }
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_Container);
                String fragmentName = f.getClass().getSimpleName();

                if ((fragmentName.equals("PassengerFragment")) || (fragmentName.equals("DriverFragment"))){
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
                else {
                    onBackPressed();
                }
                break;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed(){
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_Container);
        String fragmentName = f.getClass().getSimpleName();

        Log.d("Fragment name", "name = " + fragmentName);

        if ((fragmentName.equals("PassengerFragment")) || (fragmentName.equals("DriverFragment"))){

            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

        else if (((fragmentName.equals("ProfileFragment")) || (fragmentName.equals("ManageBookingFragment"))
                || (fragmentName.equals("HistoryFragment")) || (fragmentName.equals("BookingFragment"))
                || (fragmentName.equals("BookingDetails_Fragment")) || (fragmentName.equals("PickUpFragment")))
                && getSupportFragmentManager().getBackStackEntryCount() == 1){

            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Home");
            actionBarDrawerToggle.syncState();
            super.onBackPressed();
        }
        else if ((fragmentName.equals("BookingDetails_Fragment")) && getSupportFragmentManager().getBackStackEntryCount() > 1){
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Manage Booking");
            actionBarDrawerToggle.syncState();
            super.onBackPressed();

        } else if (fragmentName.equals("ProfileEditableFragment")) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Profile");
            actionBarDrawerToggle.syncState();
            super.onBackPressed();
        }

        else{
            super.onBackPressed();
        }
    }

    @Override
    public void onLocationFragmentInteraction(String location, int flag) {

        if (flag == 1) {
            EditText et_Origin = (EditText) findViewById(R.id.et_From);
            et_Origin.setText(location);
            List<Address> addressList = null;

            if (location != null || location.equals("")) {

                Geocoder geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    addressList = geocoder.getFromLocationName(location, 1);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (flag == 2) {
            EditText et_Destination = (EditText) findViewById(R.id.et_To);
            et_Destination.setText(location);
            List<Address> addressList = null;


            if (location != null || location.equals("")) {

                Geocoder geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    addressList = geocoder.getFromLocationName(location, 1);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onSetDateFragmentInteraction(Calendar cal, int flag) {
        cal.setTime(cal.getTime());

        SimpleDateFormat dayFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String time;

        EditText mEt_Date = (EditText) findViewById(R.id.bet_date);
        time = dayFormat.format(cal.getTime());

        if (flag == 1) {
            mEt_Date.setText(time);
        }
    }

    @Override
    public void onTimeFragmentInteraction(String time, int flag) {

        EditText mEt_Start = (EditText) findViewById(R.id.et_StartTime);
        EditText mEt_End = (EditText) findViewById(R.id.et_EndTime);

        if (flag == 1)
            mEt_Start.setText(time);
        else
            mEt_End.setText(time);

    }

    @Override
    public void onFragmentChanged(int flag, List<String> list) {
        if (flag == 1) {
            changeFragment(new BookingDetails_Fragment("Add", new Reservation()), "New Booking", true);
        }
        else if (flag == 2) {
            changeFragment(new PickUpFragment(list), "Pick Up Passenger", true);
        }
    }

    @Override
    public void onPickUpInteraction(int flag, String username) {
        switch (flag) {
            case 1: { //Start pick up
                String message = "Driver is on the way";
                ParsePush push = new ParsePush();
                ParseQuery pQuery = ParseInstallation.getQuery();

                pQuery.whereEqualTo("username", username);
                push.sendMessageInBackground(message, pQuery);
                break;
            }
            case 2: { //Reached pick up location, notify passenger
                String message = "Driver has arrive";
                ParsePush push = new ParsePush();
                ParseQuery pQuery = ParseInstallation.getQuery();

                pQuery.whereEqualTo("username", username);
                push.sendMessageInBackground(message, pQuery);
                break;
            }
            case 3: { //Complete pick up
                Toast.makeText(this, "Trip completed", Toast.LENGTH_SHORT).show();
                changeFragment(new DriverFragment(), "Home", false);
                break;
            }
            default: break;
        }
    }

    @Override
    public void onDoneClicked(String name, String operatorNo, String phoneNo, String email) {

        tv_username.setText(name);

        super.onBackPressed();

        ProfileFragment pf = (ProfileFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_Container);
        pf.updateDetails(name, email, phoneNo, operatorNo);

        getSupportActionBar().setTitle("Profile");
        drawerLayout.closeDrawers();
    }

    @Override
    public void onEditBookingPressed(String status, Reservation reservation) {
        changeFragment(new BookingDetails_Fragment(status, reservation), "Edit Booking", true);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.user.mydriver/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }


    @Override
    public void onPlaceDetailsReady(String name, double lat, double lng, String address, int placeid) {
        dbFavouritePlaceOperations dbp = new dbFavouritePlaceOperations(this);
        sqLiteDabase = dbp.getWritableDatabase();

        String str_lat = String.valueOf(lat);
        String str_lng = String.valueOf(lng);

        // get image from drawable
        Bitmap image;
        if (name.equals("Home")) {
            image = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_home_black_18dp);
        }
        else if (name.equals("Work")) {
            image = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_work_black_18dp);
        }
        else {
            image = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_star_border_black_18dp);
        }

        // convert bitmap to byte
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        String imageinString = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

        //background task -- Add Place
        PlaceRetrieve_BgroundTask favouritePlaceRetrieveBgroundTask = new PlaceRetrieve_BgroundTask(this);
        favouritePlaceRetrieveBgroundTask.execute("add_place", name, imageinString, str_lat, str_lng, address, String.valueOf(placeid));
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction2 = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.user.mydriver/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction2);
        client.disconnect();
    }

    @Override
    public void onBookingConfirmed() {
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        getSupportActionBar().setTitle("Home");
        drawerLayout.closeDrawers();
    }
}
