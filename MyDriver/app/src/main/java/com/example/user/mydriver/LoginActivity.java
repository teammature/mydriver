package com.example.user.mydriver;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.parse.ParseUser;

import fragment.LoginFragment;
import fragment.RegisterFragment;
import fragment.Profile.resetPwdFragment;

public class LoginActivity extends AppCompatActivity implements
        LoginFragment.OnLoginFragmentInteractionListener,
        LoginLayoutFragment.OnFragmentInteractionListener{

    FragmentTransaction fragmentTransaction;
    private Toolbar toolbar;
    private ParseUser loggedUser;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_launcher);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.launchercontainer, new LoginLayoutFragment(), "frontpage");
        fragmentTransaction.commit();
        getSupportActionBar().hide();

    }

    @Override
    public void onBackPressed(){
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.launchercontainer);
        String fragmentName = f.getClass().getSimpleName();

        Log.d("Fragment name", "name = " + fragmentName);

        if (fragmentName.equals("LoginLayoutFragment")){
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
        else if ((fragmentName.equals("RegisterFragment")) || (fragmentName.equals("LoginFragment"))){
            super.onBackPressed();
        }
    }

    @Override
    public void onClickLoginSuccess(ParseUser user) {
        loggedUser = user;
        finish();
    }

    @Override
    public void onClickRegisterButton() {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.launchercontainer, new RegisterFragment(), "Whole");
        fragmentTransaction.commit();
        getSupportActionBar().hide();
    }

    @Override
    public void onClickForgetPwd() {
        //display dialog
        DialogFragment newFragment = new resetPwdFragment("Reset Password", this.getBaseContext());
        newFragment.show(getFragmentManager(), "dialog");
    }


    @Override
    public void onSignInButtonPressed() {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.launchercontainer, new LoginFragment(), "Whole");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        getSupportActionBar().hide();
    }

    @Override
    public void onSignUpButtonPressed() {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.launchercontainer, new RegisterFragment(), "Whole");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        getSupportActionBar().hide();
    }

    @Override
    public void finish() {
        // Prepare data intent
        Intent data = new Intent();
        data.putExtra("loggedUser", loggedUser.getUsername());
        // Activity finished ok, return the data
        setResult(RESULT_OK, data);
        super.finish();
    }
}
